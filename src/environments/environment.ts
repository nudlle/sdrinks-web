// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { version } from '../../package.json';

export const environment = {
  production: false,
  serverUrl: 'https://localhost:8443/',
  // serverUrl: 'https://192.168.88.243/',
  // serverUrl: 'https://sdrinks.ru/',
  userMessages: 'user-messages',
  admin: 'admin',
  auth: 'auth',
  comments: 'comments',
  recipes: 'recipes',
  resources: 'resources',
  ad: 'ad',
  subscriptions: 'subscriptions',
  uploads: 'uploads',
  users: 'users',
  avatars: 'avatars',
  version: `Web-${version}`,
  license: 'license'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
