import {Component, OnInit} from '@angular/core';
import {RestUtilService} from '../service/rest-util.service';
import {User} from '../domain/user';
import {LoginService} from '../service/login.service';
import {environment} from '../../environments/environment';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RecipeResolverService} from '../service/recipe-resolver.service';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;
  resourcesUrl = `${environment.serverUrl}${environment.resources}/${environment.avatars}`;
  loading = true;
  avatarGroup: FormGroup;
  avatarInput: FormControl;
  downloadAvatarValue: number;
  loadingFile = false;
  error = 'none';

  initControls() {
    this.avatarInput = new FormControl('', Validators.required);
  }

  initGroup() {
    this.avatarGroup = new FormGroup({
      avatarInput: this.avatarInput
    });
  }

  constructor(private http: RestUtilService) {
    this.user = http.getLoginService().user;
    if (!this.user) {
      const userId = localStorage.getItem('userId');
      if (userId || LoginService.userLoggedIn) {
        this.http.getUserImpl(userId, data => {
          this.user = data;
        }, code => {
          window.alert(`Ошибка: ${code}`);
        }, () => this.loading = false, () => this.loading = false);
      } else {
        this.http.getRouter().navigate(['auth']).then();
      }
    } else {
      this.loading = false;
    }
  }

  ngOnInit() {
    this.initControls();
    this.initGroup();
  }

  onFileChange(fileInput: FileList) {
    this.error = 'none';
    this.avatarInput.setValue(fileInput.item(0));
    this.changeAvatar();
  }

  changeAvatar() {
    const formData = new FormData();
    formData.append('file', this.avatarInput.value);
    this.loadingFile = !this.loadingFile;
    this.http.uploadUserAvatarImpl(this.user.userId, formData, res => {
      this.user.avatarUrl = res;
    }, progress => {
      this.downloadAvatarValue = progress;
    }, errorCode => {
      this.error = RestUtilService.handleError(errorCode);
    }, error => {
      if (error.status === 401) {
        LoginService.userLoggedIn = false;
        this.http.getRouter().navigate(['auth']).then();
      }
    }, () => {
      this.loadingFile = !this.loadingFile;
      this.avatarInput.setValue('');
      this.downloadAvatarValue = 0;
    });
  }

  userExit() {
    if (window.confirm('Вы уверены?')) {
      this.http.getLoginService().logout(this.user.userId);
    }
  }

  getUserAvatarUrl() {
    if (this.user && this.user.avatarUrl) {
      return `url(${this.resourcesUrl}/${this.user.userId}/${this.user.avatarUrl})`;
    } else {
      return `url("../../assets/no_name_avatar.svg")`;
    }
  }

  getUserAvatarFontColor() {
    if (this.user && this.user.avatarUrl) {
      return '#fff';
    } else {
      return '#000';
    }
  }
  isDarkTheme() {
    return AppComponent.isDarkTheme;
  }

  checkSubscription() {
    return RecipeResolverService.checkSub(this.user);
  }
  getSub() {
    if (this.user) {
      return this.user.userSubscription;
    }
    return null;
  }
  buySubscription() {
    this.http.getRouter().navigate(['buy_subscription']).then();
  }
}
