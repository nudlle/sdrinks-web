export interface Subscription {
    subscriptionId: string;
    endDate: Date;
    price: number;
    userId: string;
    activated: boolean;
}
