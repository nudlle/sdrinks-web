import { RecipeStepCook } from './recipe-step-cook';
import { ImageUrls } from './image-urls';
import { Ad } from './ad';

export interface Recipe {
    recipeId: string;
    name: string;
    secondaryName: string;
    userId: string;
    userEmail: string;
    drinkState: string;
    drinkType: string;
    drinkTastes: string[];
    recipeSteps: RecipeStepCook[];
    ingredients: string[];
    imageUrls: ImageUrls;
    rate: number;
    commentCount: number;
    date: Date;
    enable: boolean;
    alcohol: boolean;
    score: number;
    searchIndex: string;
    ad: Ad;
    viewed: number;
    liked: number;
}
