export interface UserMessage {
    userMessageId: string;
    userId: string;
    message: string;
    appVersion: string;
    date: Date;
}
