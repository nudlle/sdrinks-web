export class DrinkState {
    public static Hot = 'Hot';
    public static Cold = 'Cold';
    public static Unspecified = 'Unspecified';
}
