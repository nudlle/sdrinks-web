export interface Ad {
    adId: string;
    recipeId: string;
    externalLink: string;
    imageUrl: string;
    description: string;
    stepPosition: number;
    clicked: number;
}
