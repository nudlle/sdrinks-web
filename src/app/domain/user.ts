import { Subscription } from './subscription';

export interface User {
    userId: string;
    email: string;
    password: string;
    avatarUrl: string;
    blacklist: boolean;
    ownRecipes: string[];
    likedRecipes: string[];
    userSubscription: Subscription;
}
