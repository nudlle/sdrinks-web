export interface ImageUrls {
    large: string;
    medium: string;
    small: string;
}
