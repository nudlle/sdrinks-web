export interface RecipeStepCook {
    recipeStepCookId: string;
    number: number;
    imageUrl: string;
    description: string;
    recipeId: string;
}
