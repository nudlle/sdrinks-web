export class DrinkTaste {
  public static Berries = 'Berries';
  public static Fruit = 'Fruit';
  public static Vegetable = 'Vegetable';
  public static Tasty = 'Tasty';
  public static  Sour = 'Sour';
  public static  Spicy = 'Spicy';
  public static Herb = 'Herb';
}
