export interface RecipeToSearch {
    search: string;
    state: string;
    type: string;
    tastes: string[];
    alcohol: boolean;
}
