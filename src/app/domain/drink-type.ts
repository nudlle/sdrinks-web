export class DrinkType {
  public static Lemonade = 'Lemonade';
  public static Tea = 'Tea';
  public static Coffee = 'Coffee';
  public static Shot = 'Shot';
  public static Cocktail = 'Cocktail';
  public static Milkshake = 'Milkshake';
  public static Liqueur = 'Liqueur';
}
