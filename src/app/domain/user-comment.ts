export interface UserComment {
    commentId: string;
    userId: string;
    recipeId: string;
    userEmail: string;
    comment: string;
    rate: number;
    date: Date;
}
