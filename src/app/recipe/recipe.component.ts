import {AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Recipe} from '../domain/recipe';
import {RestUtilService} from '../service/rest-util.service';
import {DrinkTaste} from '../domain/drink-taste';
import {RecipeResolverService} from '../service/recipe-resolver.service';
import {DrinkType} from '../domain/drink-type';
import {DrinkState} from '../domain/drink-state';
import {Animations} from '../animations/animations';
import {AppComponent} from '../app.component';
import {RecipeStepCook} from '../domain/recipe-step-cook';
import {ServerResponseCode} from '../response/server-response-code';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {LoginService} from '../service/login.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserComment} from '../domain/user-comment';
import {RecipesService} from '../service/recipes.service';
import {CommentsComponent} from './comments/comments.component';
import {BindData, NavBarService} from '../service/nav-bar.service';
import {ErrorMessages} from '../utils/error-messages';
import {CacheService} from '../service/cache.service';
import {CanExitGuard} from '../create-recipe/create-recipe.component';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
  animations: Animations.animationList
})
export class RecipeComponent implements OnInit, AfterViewInit, OnDestroy, CanExitGuard {
  loading = true;
  recipe: Recipe = null;
  loadFailed = false;
  isNotCommentAndNotOwn = true;
  userCommentData: DialogData;
  canExitFromPreview = false;

  ingredientsDivideCount = 10;
  ingredientsArray = new Array<string[]>();
  recipeStepsArray = new Array<RecipeStepCook>();

  rightSvgOptions = {strokeWidth: 2, type: 'left',
    strokeDashArray: '10 5', strokeLinecap: 'round', strokeOpacity: 1};
  leftSvgOptions = {strokeWidth: 2, type: 'right',
    strokeDashArray: '10 5', strokeLinecap: 'round', strokeOpacity: 1};
  showComments = false;
  @ViewChild(CommentsComponent, { static: false }) commentsComponent;
  @Input() recipePreview: boolean;

  ngAfterViewInit(): void {
  }
  getMargin() {
    return `${document.querySelector('.svg-container').clientWidth}px`;
  }
  ngOnDestroy(): void {
    this.navBarService.setActivatedRecipe(null);
  }

  private divideIngredients() {
    const d = this.recipe.ingredients.length % this.ingredientsDivideCount;
    const count = this.recipe.ingredients.length / this.ingredientsDivideCount;
    let i = 0;
    while (i < count) {
      const subArray = [];
      let offset = 0;
      while (offset < this.ingredientsDivideCount) {
        subArray.push(this.recipe.ingredients[offset + (i * this.ingredientsDivideCount)]);
        offset++;
      }
      this.ingredientsArray.push(subArray);
      i++;
    }
    if (d !== 0) {
      const subArray = [];
      let offset = count * i;
      while (offset < this.recipe.ingredients.length) {
        subArray.push(this.recipe.ingredients[offset]);
        offset++;
      }
    }
  }
  constructor(private http: RestUtilService, private snackBar: MatSnackBar,
              private dialog: MatDialog, private recipesService: RecipesService,
              private navBarService: NavBarService, private cache: CacheService) {
    this.recipePreview = this.cache.getFromCache(CacheService.RECIPE_PREVIEW) === 1;
    const url = this.http.getRouter().url;
    const recipeId = url.substring(url.lastIndexOf('/') + 1);
    if (recipeId) {
      this.http.getRecipeByIdImpl(recipeId, data => {
          this.recipe = data;
          this.navBarService.setActivatedRecipe(this.recipe);
          this.divideIngredients();
          this.fillRecipeSteps();
          this.checkCommentAlreadyPosted();
        }, () => { this.loadFailed = true; },
        () => { this.loadFailed = true; this.loading = false; }, () => { this.loading = false; });
    } else {
      this.loading = false;
      this.loadFailed = true;
    }
  }

  ngOnInit() {
  }

  fillRecipeSteps() {
    const ad = this.recipe.ad;
    this.recipe.recipeSteps.forEach(v => {
      this.recipeStepsArray.push(v);
      if (ad && ad.stepPosition === v.number) {
        this.recipeStepsArray.push(null);
      }
    });
  }

  adClicked() {
    const usrId = this.http.getLoginService().user ? this.http.getLoginService().user.userId : 'unknown';
    if (this.recipe.userId !== usrId) {
      this.http.incRecipeAdClickedImpl(this.recipe.ad.adId, () => {});
    }
    window.open(this.recipe.ad.externalLink);
  }

  getRecipeImage() {
    if (this.recipe) {
      return `url(${RecipeResolverService.getRecipeImageSource(this.recipe)})`;
    }
    return '';
  }

  getRecipeStepImageSource(step: RecipeStepCook) {
    return RecipeResolverService.getRecipeStepImageSource(step);
  }

  getRecipeAdImageSource() {
    return RecipeResolverService.getRecipeAdImageSource(this.recipe.ad);
  }

  getRecipeTaste(taste: DrinkTaste) {
    return RecipeResolverService.getRecipeTaste(taste, false);
  }

  getRecipeTasteText(taste: DrinkTaste) {
    return RecipeResolverService.getRecipeTasteText(taste);
  }

  getRecipeType(type: DrinkType) {
    return RecipeResolverService.getRecipeType(type, false);
  }

  getRecipeTypeText(type: DrinkType) {
    return RecipeResolverService.getRecipeTypeText(type);
  }

  getRecipeState(state: DrinkState) {
    return RecipeResolverService.getRecipeState(state, false);
  }

  getRecipeStateText(state: DrinkState) {
    return RecipeResolverService.getRecipeStateText(state);
  }

  transformEmail(email: string) {
    return RecipeResolverService.transformUserEmail(email);
  }

  isDarkTheme() {
    return AppComponent.isDarkTheme;
  }

  isNotLikedRecipeAndNotOwn() {
    const usr = this.http.getLoginService().user;
    const url = this.http.getRouter().url;
    const recipeId = url.substring(url.lastIndexOf('/') + 1);
    if (usr) {
      return usr.likedRecipes.indexOf(recipeId) === -1; // todo uncomment -> && usr.userId !== this.recipe.userId;
    }
    return true;
  }

  putRecipeToLiked() {
    const usr = this.http.getLoginService().user;
    if (!usr) {
      LoginService.redirectTo = this.http.getRouter().url;
      this.canExitFromPreview = true;
      this.http.getRouter().navigate(['auth']).then();
    } else {
      this.http.addLikedRecipeImpl(usr.userId, this.recipe.recipeId, code => {
        if (code === ServerResponseCode.OK_RESPONSE) {
          usr.likedRecipes.push(this.recipe.recipeId);
          this.snackBar.open('Рецепт добавлен в избранное', 'OK', { duration: 2000 });
        } else {
          this.snackBar.open(`Ошибка: ${code}`, 'OK', { duration: 2000 });
        }
      }, error => {
        if (error.status === 401) {
          LoginService.userLoggedIn = false;
          LoginService.redirectTo = this.http.getRouter().url;
          this.canExitFromPreview = true;
          this.http.getRouter().navigate(['auth']).then();
        }
      }, () => {
      });
    }
  }
  private checkCommentAlreadyPosted() {
    const user = this.http.getLoginService().user;
    if (user) {
      this.http.isCommentAlreadyPostedImpl(this.recipe.recipeId, user.userId, code => {
        this.isNotCommentAndNotOwn = code !== ServerResponseCode.COMMENT_ALREADY_POSTED;
      });
      // if (this.user.userId !== this.recipe.userId) {
      //   this.http.isCommentAlreadyPostedImpl(this.recipe.recipeId, this.user.userId, code => {
      //     this.isNotCommentAndNotOwn = code !== ServerResponseCode.COMMENT_ALREADY_POSTED;
      //   }, error => {}, () => {});
      // } else {
      //   this.isNotCommentAndNotOwn = false;
      // }
    } else {
      this.isNotCommentAndNotOwn = true;
    }
  }
  commentRecipe() {
    if (this.http.getLoginService().user) {
      this.openPostCommentDialog();
    } else {
      LoginService.redirectTo = this.http.getRouter().url;
      this.canExitFromPreview = true;
      this.http.getRouter().navigate(['auth']).then();
    }
  }
  isNotCommented() {
    return this.isNotCommentAndNotOwn;
  }
  private openPostCommentDialog() {
    const dialogRef = this.dialog.open(CommentRecipeDialogComponent, {
      width: '90vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.userCommentData = result;
        this.sendComment();
      }
    });
  }
  private sendComment() {
    const comment: UserComment = {
      commentId: undefined,
      userId: this.http.getLoginService().user.userId,
      recipeId: this.recipe.recipeId,
      userEmail: this.http.getLoginService().user.email,
      comment: this.userCommentData.comment,
      rate: this.userCommentData.rate,
      date: undefined
    };
    this.http.createCommentImpl(comment, uc => {
      this.recipe.commentCount++;
      this.recipe.rate += uc.rate;
      this.isNotCommentAndNotOwn = false;
      this.recipesService.updateRecipe(this.recipe);
      if (this.commentsComponent) {
        this.commentsComponent.addNewComment(uc);
      }
      this.snackBar.open('Комментарий успешно добавлен', 'OK', { duration: 2000 });
    }, code => {
      this.snackBar.open(`Ошибка добавления комментария: ${code}`, 'ОК', { duration: 2000 });
    }, error => {
      if (error.status === 401) {
        LoginService.userLoggedIn = false;
        LoginService.redirectTo = this.http.getRouter().url;
        this.canExitFromPreview = true;
        this.http.getRouter().navigate(['auth']).then();
      }
    });
  }
  hideComments() {
    this.showComments = false;
  }
  showCommentsImpl() {
    if (!this.recipePreview) {
      this.navBarService.emitEvent(BindData.ScrollTop);
      this.showComments = true;
    }
  }
  exitFromPreview(navigate: string) {
    this.http.getRouter().navigate([navigate]).then();
    this.cache.putToCache(CacheService.RECIPE_PREVIEW, 0);
  }
  activateRecipe() {
    if (this.recipePreview) {
      this.http.activateRecipeImpl(this.recipe.recipeId, this.recipe.userId, code => {
        if (code === ServerResponseCode.OK_RESPONSE) {
          this.snackBar.open('Рецепт успешно добавлен', 'OK', {duration: 2000});
        } else {
          this.snackBar.open(`Не удалось добавить рецепт: ${RestUtilService.handleError(code)}`, 'OK', {duration: 2000});
        }
        this.canExitFromPreview = true;
        this.exitFromPreview('recipes');
      }, error => {
        this.snackBar.open(`Ошибка: ${error}`, 'OK', {duration: 2000});
        this.exitFromPreview('recipes');
      });
    }
  }
  cancelRecipe() {
    this.canExitFromPreview = true;
    this.http.deleteRecipeImpl(this.recipe.userId, this.recipe.recipeId, () => {
      this.exitFromPreview('recipes/create');
    }, () => { this.exitFromPreview('recipes/create'); });
  }

  canExit(): boolean {
    return this.canExitFromPreview;
  }
}
export interface DialogData {
  rate: number;
  comment: string;
}
@Component({
  selector: 'app-comment-recipe-dialog',
  styleUrls: ['./comment-recipe-dialog.scss'],
  templateUrl: './comment-recipe-dialog.html',
})
export class CommentRecipeDialogComponent implements OnInit {
  commentRecipeGroup: FormGroup;
  comment: FormControl;
  rate = 5;
  initGroup() {
    this.commentRecipeGroup = new FormGroup({
      comment: this.comment
    });
  }
  initFormControl() {
    this.comment = new FormControl('', [Validators.required, Validators.maxLength(500)]);
  }
  constructor(public dialogRef: MatDialogRef<CommentRecipeDialogComponent>) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.initFormControl();
    this.initGroup();
  }
  getData() {
    return { comment: this.comment.value, rate: this.rate };
  }
  getCommentErrorMessage() {
    return this.comment.hasError('required') ? ErrorMessages.ErrorRequired :
      this.comment.hasError('maxlength') ? ErrorMessages.errorMaxLength(500) : '';
  }
  check() {
    const tmp = this.comment.value.toString();
    return tmp.trim().length === 0;
  }
}
