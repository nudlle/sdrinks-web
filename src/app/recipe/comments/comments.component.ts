import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {UserComment} from '../../domain/user-comment';
import {RestUtilService} from '../../service/rest-util.service';
import {Subscription} from 'rxjs';
import {ServerResponseCode} from '../../response/server-response-code';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/overlay';
import {Animations} from '../../animations/animations';
import {RecipeResolverService} from '../../service/recipe-resolver.service';
import {AppComponent} from '../../app.component';
import {BindData, NavBarService} from '../../service/nav-bar.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  animations: Animations.animationList
})
export class CommentsComponent implements OnInit, AfterViewInit, OnDestroy {
  allCommentsArray: UserComment[] = [];
  @Input() recipeId: string;
  @Input() commentCount: number;
  @Input() rn: string;
  @Input() rsn: string;
  @Output() goBackCalled: EventEmitter<any> = new EventEmitter();
  loading = false;
  loadingNextPage = false;
  scrollSub: Subscription = null;
  commentsPageSize = 120;
  goBackSubs: Subscription = null;

  constructor(private http: RestUtilService, private scrolling: ScrollDispatcher,
              private navBarService: NavBarService) {
    this.navBarService.setCommentActivated(true);
    this.goBackSubs = this.navBarService.subscribe(data => {
      if (data === BindData.HideComments) {
        this.goBackImpl();
      }
    });
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    if (this.commentCount !== 0) {
      this.loading = true;
      this.loadNextPage(() => this.loading = false,
        () => { this.loading = false; }, () => {
          this.loading = false;
        });
      this.scrollSub = this.scrolling.scrolled().subscribe((data: CdkScrollable) => {
          this.windowScrolling(data);
      });
    }
  }
  ngOnDestroy(): void {
    if (this.scrollSub) {
      this.scrollSub.unsubscribe();
    }
    if (this.goBackSubs) {
      this.goBackSubs.unsubscribe();
    }
  }

  private windowScrolling(data: CdkScrollable) {
    const scrollTop = data.getElementRef().nativeElement.scrollTop;
    const offsetHeight = data.getElementRef().nativeElement.offsetHeight;
    const scrollHeight = data.getElementRef().nativeElement.scrollHeight;
    if (scrollTop + offsetHeight >= scrollHeight && !this.loadingNextPage) {
      this.loadingNextPage = true;
      if (this.doLoad()) {
        this.loadNextPage(() => this.loadingNextPage = false, () => { this.loadingNextPage = false; }, () => {
          this.loadingNextPage = false;
        });
      } else {
        this.loadingNextPage = false;
      }
    }
  }

  doLoad() {
    return this.allCommentsArray.length % this.commentsPageSize === 0;
  }

  getNextPage() {
    const dd = this.allCommentsArray.length % this.commentsPageSize;
    if (dd === 0) {
      return this.allCommentsArray.length / this.commentsPageSize;
    }
    return 0;
  }

  transformEmail(email: string) {
    return RecipeResolverService.transformUserEmail(email);
  }

  loadNextPage(errorCode: (code: ServerResponseCode) => void,
               exception: (error: Error) => void,
               complete: () => void) {
    this.http.getCommentsOfRecipeImpl(this.recipeId, this.getNextPage(), this.commentsPageSize, data => {
      this.allCommentsArray.push(data);
    }, errorCode, exception, complete);
  }
  goBackImpl() {
    this.navBarService.emitEvent(BindData.ScrollTop);
    this.loading = true;
    setTimeout(() => {
      this.goBackCalled.emit(true);
    }, 500);
    this.navBarService.setCommentActivated(false);
  }
  public addNewComment(comment: UserComment) {
    this.allCommentsArray.unshift(comment);
  }
}
