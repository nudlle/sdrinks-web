import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {finalize, tap} from 'rxjs/operators';


@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // console.log(`Request Header - X-Requested-With: ${req.headers.get('X-Requested-With')}`);
    // console.log(`Request Header - Authorization: ${req.headers.get('Authorization')}`);
    const startTime = Date.now();
    let status: string;

    return next.handle(req).pipe(
      tap(
        event => {
          status = '';
          if (event instanceof HttpResponse) {
            status = 'succeeded';
          }
        },
        error => {
          status = 'failed';
          // console.log(`Response Headers WWW-Authenticate: ${error.headers.get('WWW-Authenticate')}`);
        }
      ),
      finalize(() => {
        const elapsedTime = Date.now() - startTime;
        const message = req.method + ' ' + req.urlWithParams + ' ' + status
          + ' in ' + elapsedTime + 'ms';

        this.logDetails(message);
      })
    );
  }
  private logDetails(msg: string) {
    console.log(msg);
  }
}
