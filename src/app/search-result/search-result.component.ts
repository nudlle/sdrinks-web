import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {CacheService} from '../service/cache.service';
import {RecipesService} from '../service/recipes.service';
import {Subscription} from 'rxjs';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/overlay';
import {DrinkTaste} from '../domain/drink-taste';
import {RecipeImageType, RecipeResolverService} from '../service/recipe-resolver.service';
import {DrinkType} from '../domain/drink-type';
import {DrinkState} from '../domain/drink-state';
import {Recipe} from '../domain/recipe';
import {NavBarService} from '../service/nav-bar.service';
import {AppComponent} from '../app.component';
import {Animations} from '../animations/animations';
import {RecipeToSearch} from '../domain/recipe-to-search';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
  animations: Animations.animationList
})
export class SearchResultComponent implements OnInit, AfterViewInit, OnDestroy {
  private loading = true;
  private loadingNextPage = false;
  private scrollSub: Subscription = null;
  private readonly searchSub: Subscription = null;
  private noContent = true;

  constructor(private scrolling: ScrollDispatcher,
              private recipeService: RecipesService,
              private cache: CacheService,
              private navBarService: NavBarService) {
    this.searchRecipes(this.cache.getSearchCache());
    this.searchSub = this.navBarService.subscribeSearch(data => {
      if (data) {
        this.searchRecipes(data);
      }
    });
  }
  private searchRecipes(rs: RecipeToSearch) {
    if (rs) {
      this.loading = true;
      this.recipeService.searchedRecipes = [];
      this.recipeService.loadNextSearchPage(rs,
        () => { this.loading = false; },
        () => { this.loading = false; },
        () => {
        this.loading = false;
        this.noContent = this.getRecipes().length === 0;
      });
    } else {
      this.loading = false;
      this.noContent = true;
    }
  }

  private windowScrolling(data: CdkScrollable) {
    const scrollTop = data.getElementRef().nativeElement.scrollTop;
    const offsetHeight = data.getElementRef().nativeElement.offsetHeight;
    const scrollHeight = data.getElementRef().nativeElement.scrollHeight;
    if (scrollTop + offsetHeight >= scrollHeight && !this.loadingNextPage) {
      this.loadingNextPage = true;
      if (this.recipeService.doLoadSearch()) {
        this.recipeService.loadNextSearchPage(this.cache.getSearchCache(), ()  => { this.loadingNextPage = false; },
          () => { this.loadingNextPage = false; },
           () => {
          this.loadingNextPage = false;
        });
      } else {
        this.loadingNextPage = false;
      }
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.scrollSub = this.scrolling.scrolled().subscribe((data: CdkScrollable) => {
      this.windowScrolling(data);
    });
  }

  ngOnDestroy(): void {
    if (this.scrollSub) {
      this.scrollSub.unsubscribe();
    }
    if (this.searchSub) {
      this.scrollSub.unsubscribe();
    }
  }

  getRecipes() {
    return this.recipeService.searchedRecipes;
  }

  getRecipeTaste(taste: DrinkTaste) {
    return RecipeResolverService.getRecipeTaste(taste);
  }

  getRecipeType(type: DrinkType) {
    return RecipeResolverService.getRecipeType(type);
  }

  getRecipeState(state: DrinkState) {
    return RecipeResolverService.getRecipeState(state);
  }

  transformEmail(email: string) {
    return RecipeResolverService.transformUserEmail(email);
  }
  openRecipe(index: number, recipe: Recipe) {
    this.recipeService.openRecipe(index, recipe);
  }
  getRecipeBackgroundImage(recipe: Recipe) {
    return `url(${RecipeResolverService.getRecipeImageSource(recipe, RecipeImageType.SMALL)})`;
  }
  isDarkTheme() {
    return AppComponent.isDarkTheme;
  }
  goBack() {
    this.navBarService.returnBackFromSearch();
  }
}
