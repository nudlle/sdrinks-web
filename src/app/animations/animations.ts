import {animate, animateChild, group, query, stagger, state, style, transition, trigger} from '@angular/animations';

export const Animations = {
  animationList: [
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(50, animateChild()), {optional: true})
      ]),
    ]),
    trigger('items', [
      transition(':enter', [
        style({transform: 'scale(0.5)', opacity: 0}),  // initial
        animate('.5s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({transform: 'scale(1)', opacity: 1}))  // final
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: 1, height: '*'}),
        animate('.3s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.5)', opacity: 0,
            height: '0px', margin: '0px'
          }))
      ])
    ]),
    trigger('click', [
      state('non_clicked', style({transform: 'scale(1)'})),
      state('clicked', style({transform: 'scale(1.5)'})),
      transition('non_clicked => clicked', [
        style({transform: 'scale(.5)'}),
        animate('.5s')
      ]),
      transition('clicked => non_clicked', [
        animate('.5s')
      ])
    ]),
    trigger('recipe', [
      transition(':enter', [
        query('@recipe-info', stagger(100, animateChild()), {optional: true})
      ])
    ]),
    trigger('recipe-info', [
      transition(':enter', [
        style({transform: 'scale(0.1)', opacity: '0'}),
        animate('.7s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(1)', opacity: '1'
          }))
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: '1'}),
        animate('.7s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.1)', opacity: '0'
          }))
      ])
    ]),
    trigger('recipe-info-to-comments', [
      transition(':enter', [
        style({transform: 'scale(0.1)', opacity: '0'}),
        animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(1)', opacity: '1'
          }))
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: '1'}),
        animate('.5s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.1)', opacity: '0'
          }))
      ])
    ]),
    trigger('comments-list', [
      transition(':enter', [
        query('@comments-items', stagger(100, animateChild()), {optional: true})
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: '1'}),
        animate('.5s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.1)', opacity: '0'
          })),
      ])
    ]),
    trigger('comments-items', [
      transition(':enter', [
        style({transform: 'scale(0.5)', opacity: 0}),  // initial
        animate('.7s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({transform: 'scale(1)', opacity: 1}))  // final
      ]),
      transition(':leave', [
        style({transform: 'scale(1)', opacity: 1, height: '*'}),
        animate('.3s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.5)', opacity: 0,
            height: '0px', margin: '0px'
          }))
      ])
    ]),
  ],
  // animationClick: [
  //   trigger('click', [
  //     transition('* => clicked', [
  //       style({transform: 'scale(.5)'}),
  //       animate('1s easy-in-out', style({transform: 'scale(5)'}))
  //     ]),
  //     transition('clicked => *', [
  //       style({transform: 'scale(5)'}),
  //       animate('1s easy-in-out', style({transform: 'scale(1)'}))
  //     ])
  //   ]),
  // ]
};
