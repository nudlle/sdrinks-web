import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RecipeStepCook} from '../../domain/recipe-step-cook';
import {MatSnackBar} from '@angular/material';
import {ErrorMessages} from '../../utils/error-messages';
import {RecipeResolverService} from '../../service/recipe-resolver.service';
import {Cached} from '../create-recipe.component';
import {CacheService} from '../../service/cache.service';
import {Utils} from '../../utils/utils';

export interface RecipeCreateSteps {
  stepFiles: any;
  loadStepFiles: any;
  recipeSteps: RecipeStepCook[];
}
@Component({
  selector: 'app-recipe-steps',
  templateUrl: './recipe-steps.component.html',
  styleUrls: ['./recipe-steps.component.scss']
})
export class RecipeStepsComponent implements OnInit, Cached {
  recipeStepFile: any;
  loadStepFile: any;
  recipeStepDescGroup: FormGroup;
  recipeStepDesc: FormControl;
  recipeStepFiles: any[] = [];
  loadStepFiles: any[] = [];
  recipeSteps: RecipeStepCook[] = [];
  imagePathCash = new Map<number, any>();
  @ViewChild('file', {static: false}) rsf: ElementRef;

  constructor(private snackBar: MatSnackBar, private cache: CacheService) { }

  private initFormControls() {
    this.recipeStepDesc = new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(500)]);
  }
  private initFormGroups() {
    this.recipeStepDescGroup = new FormGroup({
      recipeStepDesc: this.recipeStepDesc
    });
  }

  ngOnInit() {
    this.initFormControls();
    this.initFormGroups();
    this.restoreData();
  }

  showPrevRecipeStepImage(event: FileList) {
    if (event && event.item(0)) {
      RecipeResolverService.loadFile(event, result => { this.recipeStepFile = result; }, item => { this.loadStepFile = item; });
    }
  }

  validRecipeStep(): boolean {
    if (Utils.checkWhitespaces(this.recipeStepDesc.value)) {
      this.recipeStepDesc.setValue('');
      this.recipeStepDesc.markAsTouched();
      return false;
    } else if (this.recipeStepDesc.invalid) {
      this.recipeStepDesc.markAsTouched();
      return false;
    }
    if (!this.recipeStepFile) {
      this.snackBar.open('Выберите изображение для описания шага', 'OK', {duration: 3000});
      return false;
    }
    return true;
  }

  addRecipeStep() {
    if (this.validRecipeStep()) {
      const recipeStep = {
        recipeStepCookId: undefined,
        number: undefined,
        imageUrl: undefined,
        description: this.recipeStepDesc.value,
        recipeId: undefined
      };
      this.recipeSteps.push(recipeStep);
      this.recipeStepFiles.push(this.recipeStepFile);
      this.loadStepFiles.push(this.loadStepFile);
      this.recipeStepFile = null;
      this.loadStepFile = null;
      this.rsf.nativeElement.value = '';
      this.recipeStepDescGroup.reset();
    }
  }

  removeRecipeStep(index: number) {
    this.recipeStepFiles.splice(index, 1);
    this.recipeSteps.splice(index, 1);
  }

  getErrorMsgRecipeStepDesc() {
    return this.recipeStepDesc.hasError('required') ? ErrorMessages.ErrorRequired :
      this.recipeStepDesc.hasError('minlength') ? ErrorMessages.errorMinLength(10) :
        this.recipeStepDesc.hasError('maxlength') ? ErrorMessages.errorMaxLength(500) : '';
  }

  public isValid() {
    if (this.recipeSteps.length === 0) {
      this.snackBar.open('Добавьте шаги приготовления', 'OK', {duration: 3000});
      return false;
    }
    return true;
  }
  public getSteps() {
    this.imagePathCash.clear();
    this.recipeSteps.forEach( (s, i) => {
      s.number = i + 1;
      this.imagePathCash.set(s.number, this.loadStepFiles[i]);
    });
    return this.recipeSteps;
  }

  cacheData(): any {
    return {
      stepFiles: this.recipeStepFiles,
      loadStepFiles: this.loadStepFiles,
      recipeSteps: this.recipeSteps
    };
  }
  restoreData(): void {
    if (this.cache.getFromCacheR()) {
      const steps = this.cache.getFromCacheR().recipeSteps;
      if (steps) {
        this.recipeStepFiles = steps.stepFiles;
        this.loadStepFiles = steps.loadStepFiles;
        this.recipeSteps = steps.recipeSteps;
      }
    }
  }
}
