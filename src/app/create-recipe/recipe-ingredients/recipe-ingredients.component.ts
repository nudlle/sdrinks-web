import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessages} from '../../utils/error-messages';
import {MatSnackBar} from '@angular/material';
import {Cached} from '../create-recipe.component';
import {CacheService} from '../../service/cache.service';
import {Utils} from '../../utils/utils';

export interface RecipeCreateIngredients {
  ingredients: string[];
}
@Component({
  selector: 'app-recipe-ingredients',
  templateUrl: './recipe-ingredients.component.html',
  styleUrls: ['./recipe-ingredients.component.scss']
})
export class RecipeIngredientsComponent implements OnInit, Cached {
  ingredients: string[] = [];
  ingredientGroup: FormGroup;
  ingredient: FormControl;

  private initFormControls() {
    this.ingredient = new FormControl('', [Validators.required, Validators.maxLength(35)]);
  }
  private initFormGroups() {
    this.ingredientGroup = new FormGroup({
      ingredient: this.ingredient
    });
  }

  constructor(private snackBar: MatSnackBar, private cache: CacheService) { }

  ngOnInit() {
    this.initFormControls();
    this.initFormGroups();
    this.restoreData();
  }

  addIngredient() {
    if (Utils.checkWhitespaces(this.ingredient.value)) {
      this.ingredient.setValue('');
      this.ingredient.markAsTouched();
    } else if (this.ingredientGroup.valid) {
      this.ingredients.push(this.ingredient.value);
      this.ingredientGroup.reset();
    }
  }
  deleteIngredient(i: number) {
    this.ingredients.splice(i, 1);
  }

  getErrorMsgIngredient() {
    return this.ingredient.hasError('required') ? ErrorMessages.ErrorRequired :
      this.ingredient.hasError('maxlength') ? ErrorMessages.errorMaxLength(35) : '';
  }

  public isValid() {
    if (this.ingredients.length === 0) {
      this.snackBar.open('Добавьте ингредиенты', 'OK', {duration: 3000});
      return false;
    }
    return true;
  }

  public getIngredients() {
    return this.ingredients;
  }

  cacheData(): any {
    return { ingredients: this.ingredients };
  }

  restoreData(): void {
    if (this.cache.getFromCacheR()) {
      const recipeIngredients = this.cache.getFromCacheR().recipeIngredients;
      if (recipeIngredients) {
        this.ingredients = recipeIngredients.ingredients;
      }
    }
  }
}
