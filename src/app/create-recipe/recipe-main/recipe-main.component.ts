import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DrinkType} from '../../domain/drink-type';
import {DrinkState} from '../../domain/drink-state';
import {ErrorMessages} from '../../utils/error-messages';
import {MatSnackBar} from '@angular/material';
import {Recipe} from '../../domain/recipe';
import {DrinkTaste} from '../../domain/drink-taste';
import {RecipeResolverService} from '../../service/recipe-resolver.service';
import {Cached} from '../create-recipe.component';
import {CacheService} from '../../service/cache.service';

export interface RecipeCreateMain {
  mainImage: any;
  loadImage: any;
  recipeName: string;
  recipeSecondaryName: string;
  searchIndex: string;
  drinkTastes: Array<boolean>;
  drinkType: string;
  drinkState: string;
  alco: string;
}
@Component({
  selector: 'app-recipe-main',
  templateUrl: './recipe-main.component.html',
  styleUrls: ['./recipe-main.component.scss']
})
export class RecipeMainComponent implements OnInit, Cached {
  file: any;
  loadFile: any;
  recipeGroup: FormGroup;
  recipeName: FormControl;
  recipeSecondaryName: FormControl;
  searchIndex: string;
  drinkTastes = new Array<boolean>(7);
  drinkType = DrinkType.Lemonade;
  drinkState = DrinkState.Unspecified;
  alco = '0';

  private getDrinkTastes() {
    const tt = [];
    this.drinkTastes.forEach((t, i) => { if (t) { tt.push(RecipeResolverService.getDrinkTaste(i)); } });
    return tt;
  }
  private initFormControls() {
    this.recipeName = new FormControl('', [Validators.required, Validators.maxLength(30)]);
    this.recipeSecondaryName = new FormControl('', Validators.maxLength(30));
  }
  private initFormGroups() {
    this.recipeGroup = new FormGroup({
      recipeName: this.recipeName,
      recipeSecondaryName: this.recipeSecondaryName
    });
  }
  constructor(private snackBar: MatSnackBar, private cache: CacheService) { }

  ngOnInit() {
    this.initFormControls();
    this.initFormGroups();
    this.restoreData();
  }

  showPrevRecipeImage(event: FileList) {
    if (event && event.item(0)) {
      RecipeResolverService.loadFile(event, result => { this.file = result; }, item => { this.loadFile = item; });
    }
  }
  getErrorMsgRecipeName() {
    return this.recipeName.hasError('required') ? ErrorMessages.ErrorRequired :
      this.recipeName.hasError('maxlength') ? ErrorMessages.errorMaxLength(30) : '';
  }
  getErrorMsgRecipeSecondaryName() {
    return this.recipeSecondaryName.hasError('maxlength') ? ErrorMessages.errorMaxLength(30) : '';
  }
  private initSearchIndex() {
    let builder = `${this.drinkType} ${this.drinkState} `;
    this.drinkTastes.forEach((t, i) => { if (t) { builder += `${RecipeResolverService.getDrinkTaste(i)} `; } });
    if (this.alco === '1') { builder += 'Alco '; }
    if (this.searchIndex) {
      const index = this.searchIndex.split(',');
      index.forEach(v => { builder += `${v} `; });
    }
    return builder;
  }
  public isValid(): boolean {
    if (this.recipeName.invalid) {
      this.recipeName.markAsTouched();
      return false;
    }
    if (!this.file) {
      this.snackBar.open('Выберите изображение рецепта', 'OK', {duration: 3000});
      return false;
    }
    if (!this.drinkTastes[0] && !this.drinkTastes[1] &&
        !this.drinkTastes[2] && !this.drinkTastes[3] &&
        !this.drinkTastes[4] && !this.drinkTastes[5] && !this.drinkTastes[6]) {
      this.snackBar.open('Выберите вкус', 'OK', {duration: 3000});
      return false;
    }
    return true;
  }
  public getRecipe(): Recipe {
    return {
      recipeId: undefined,
      name: this.recipeName.value,
      secondaryName: this.recipeSecondaryName.value,
      userId: undefined,
      userEmail: undefined,
      drinkState: this.drinkState,
      drinkType: this.drinkType,
      drinkTastes: this.getDrinkTastes(),
      recipeSteps: [],
      ingredients: [],
      imageUrls: undefined,
      rate: 0,
      commentCount: 0,
      date: undefined,
      enable: false,
      alcohol: this.alco === '1',
      score: undefined,
      searchIndex: this.initSearchIndex(),
      ad: undefined,
      viewed: 0,
      liked: 0
    };
  }
  public getRecipeImage() {
    return this.file;
  }

  cacheData(): any {
    return {
      mainImage: this.file,
      loadImage: this.loadFile,
      recipeName: this.recipeName.value,
      recipeSecondaryName: this.recipeSecondaryName.value,
      searchIndex: this.searchIndex,
      drinkTastes: this.drinkTastes,
      drinkType: this.drinkType,
      drinkState: this.drinkState,
      alco: this.alco,
    };
  }
  restoreData(): void {
    if (this.cache.getFromCacheR()) {
      const cachedMain = this.cache.getFromCacheR().recipeMain;
      if (cachedMain) {
        this.file = cachedMain.mainImage;
        this.loadFile = cachedMain.loadImage;
        this.recipeName.setValue(cachedMain.recipeName);
        this.recipeSecondaryName.setValue(cachedMain.recipeSecondaryName);
        this.searchIndex = cachedMain.searchIndex;
        this.drinkTastes = cachedMain.drinkTastes;
        this.drinkType = cachedMain.drinkType;
        this.drinkState = cachedMain.drinkState;
        this.alco = cachedMain.alco;
      }
    }
  }
}
