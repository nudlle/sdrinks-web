import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessages} from '../../utils/error-messages';
import {RestUtilService} from '../../service/rest-util.service';
import {MatSnackBar} from '@angular/material';
import {RecipeResolverService} from '../../service/recipe-resolver.service';
import {Cached} from '../create-recipe.component';
import {CacheService} from '../../service/cache.service';

export interface RecipeCreateAd {
  adFile: any;
  loadAdFile: any;
  adLink: string;
  adDesc: string;
  adOn: boolean;
}
@Component({
  selector: 'app-recipe-ad',
  templateUrl: './recipe-ad.component.html',
  styleUrls: ['./recipe-ad.component.scss']
})
export class RecipeAdComponent implements OnInit, Cached {
  recipeAdFile: any;
  loadRecipeAdFile: any;
  recipeAdGroup: FormGroup;
  recipeAdLink: FormControl;
  recipeAdDesc: FormControl;
  recipeAdOn = true;
  urlPattern = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)/;
  @Output() buySub = new EventEmitter<any>();

  private initFormControls() {
    this.recipeAdLink = new FormControl('', [Validators.required, Validators.maxLength(500), Validators.pattern(this.urlPattern)]);
    this.recipeAdDesc = new FormControl('', Validators.maxLength(1000));
  }
  private initFormGroups() {
    this.recipeAdGroup = new FormGroup({
      recipeAdLink: this.recipeAdLink,
      recipeAdDesc: this.recipeAdDesc
    });
  }

  constructor(private http: RestUtilService, private cache: CacheService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initFormControls();
    this.initFormGroups();
    this.restoreData();
  }

  showPrevRecipeAdImage(event: FileList) {
    if (event && event.item(0)) {
      RecipeResolverService.loadFile(event, result => { this.recipeAdFile = result; },
          item => { this.loadRecipeAdFile = item; });
    }
  }

  getErrorMsgRecipeAdLink() {
    return this.recipeAdLink.hasError('required') ? ErrorMessages.ErrorRequired :
      this.recipeAdLink.hasError('maxlength') ? ErrorMessages.errorMaxLength(500) :
        this.recipeAdLink.hasError('pattern') ? ErrorMessages.ErrorUrl : '';
  }
  getErrorMsgRecipeAdDesc() {
    return this.recipeAdDesc.hasError('maxlength') ? ErrorMessages.errorMaxLength(1000) : '';
  }

  checkSubscription() {
    const usr = this.http.getLoginService().user;
    return RecipeResolverService.checkSub(usr);
  }

  public isValid() {
    if (!this.recipeAdOn) {
      return true;
    } else {
      if (this.recipeAdLink.invalid) {
        this.recipeAdLink.markAsTouched();
        return false;
      }
      if (!this.recipeAdFile) {
        this.snackBar.open('Выберите изображения рекламы', 'OK', {duration: 3000});
        return false;
      }
    }

    return true;
  }
  public getAd() {
    return {
      adId: undefined,
      recipeId: undefined,
      externalLink: this.recipeAdLink.value,
      imageUrl: undefined,
      description: this.recipeAdDesc.value,
      stepPosition: undefined,
      clicked: 0
    };
  }
  public getAdImage() {
    return this.loadRecipeAdFile;
  }
  public isOn() {
    return this.recipeAdOn;
  }

  cacheData(): any {
    return {
      adFile: this.recipeAdFile,
      loadAdFile: this.loadRecipeAdFile,
      adLink: this.recipeAdLink.value,
      adDesc: this.recipeAdDesc.value,
      adOn: this.recipeAdOn
    };
  }
  restoreData(): void {
    if (this.cache.getFromCacheR()) {
      const ad = this.cache.getFromCacheR().recipeAd;
      if (ad) {
        this.recipeAdFile = ad.adFile;
        this.loadRecipeAdFile = ad.loadAdFile;
        this.recipeAdLink.setValue(ad.adLink);
        this.recipeAdDesc.setValue(ad.adDesc);
        this.recipeAdOn = ad.adOn;
      }
    }
  }

  buySubscription() {
    this.buySub.emit();
  }
}
