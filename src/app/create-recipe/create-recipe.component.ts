import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {AppComponent} from '../app.component';
import {MAT_DIALOG_DATA, MatDialog, MatSnackBar} from '@angular/material';
import {LoginService} from '../service/login.service';
import {RestUtilService} from '../service/rest-util.service';
import {RecipeCreateMain, RecipeMainComponent} from './recipe-main/recipe-main.component';
import {RecipeCreateIngredients, RecipeIngredientsComponent} from './recipe-ingredients/recipe-ingredients.component';
import {RecipeCreateSteps, RecipeStepsComponent} from './recipe-steps/recipe-steps.component';
import {RecipeAdComponent, RecipeCreateAd} from './recipe-ad/recipe-ad.component';
import {Recipe} from '../domain/recipe';
import {RecipeResolverService} from '../service/recipe-resolver.service';
import {CacheService} from '../service/cache.service';

export interface CanExitGuard {
  canExit(): boolean;
}
export interface RecipeCreateCache {
  recipeMain: RecipeCreateMain;
  recipeIngredients: RecipeCreateIngredients;
  recipeSteps: RecipeCreateSteps;
  recipeAd: RecipeCreateAd;
}
export declare interface Cached {
  cacheData(): any;
  restoreData(): void;
}
@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.scss']
})
export class CreateRecipeComponent implements OnInit, AfterViewInit, CanExitGuard {
  tabIndex = 0;
  canExitFromCreate = false;
  @ViewChild(RecipeMainComponent, {static: false}) recipeMain: RecipeMainComponent;
  @ViewChild(RecipeIngredientsComponent, {static: false}) recipeIngredients: RecipeIngredientsComponent;
  @ViewChild(RecipeStepsComponent, {static: false}) recipeSteps: RecipeStepsComponent;
  @ViewChild(RecipeAdComponent, {static: false}) recipeAd: RecipeAdComponent;
  private cashCreateRecipe() {
    const cash = {
      recipeMain: this.recipeMain.cacheData(),
      recipeIngredients: this.recipeIngredients.cacheData(),
      recipeSteps: this.recipeSteps.cacheData(),
      recipeAd: this.recipeAd.cacheData()
    };
    this.cache.putToCacheR(cash);
  }
  constructor(private http: RestUtilService, private cache: CacheService, private snackBar: MatSnackBar, private dialog: MatDialog) {
    if (!LoginService.userLoggedIn) {
      LoginService.redirectTo = '/recipes/create';
      this.canExitFromCreate = true;
      this.http.getRouter().navigate(['auth']).then();
    }
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.cache.putToCacheR(null);
  }

  postDialogErrorMessage(error: string) {
    const dialogRef = this.dialog.getDialogById('loading-dialog');
    dialogRef.componentInstance.data = { message: error };
    dialogRef.disableClose = false;
  }
  postDialogMessage(msg: string) {
    const dialogRef = this.dialog.getDialogById('loading-dialog');
    dialogRef.componentInstance.data = { message: msg };
  }
  closeDialog() {
    const dialogRef = this.dialog.getDialogById('loading-dialog');
    dialogRef.close();
  }

  goNext() {
    if (!this.recipeMain.isValid()) {
      this.tabIndex = 0;
    } else if (!this.recipeIngredients.isValid()) {
      this.tabIndex = 1;
    } else if (!this.recipeSteps.isValid()) {
      this.tabIndex = 2;
    } else if (!this.recipeAd.isValid()) {
      this.tabIndex = 3;
    } else if (this.tabIndex < 3) {
      this.tabIndex++;
    } else {
      this.createRecipe();
    }
  }

  createRecipe() {
    const recipe = this.recipeMain.getRecipe();
    const usr = this.http.getLoginService().user;
    if (usr) { recipe.searchIndex += RecipeResolverService.transformUserEmail(usr.email); }
    recipe.userId = usr.userId;
    recipe.userEmail = usr.email;
    recipe.ingredients = this.recipeIngredients.getIngredients();
    recipe.recipeSteps = this.recipeSteps.getSteps();
    if (this.recipeAd.isOn()) {
      const ad = this.recipeAd.getAd();
      ad.stepPosition = recipe.recipeSteps.length === 1 ? 1 : recipe.recipeSteps.length / 2;
      recipe.ad = ad;
    } else {
      recipe.ad = null;
    }

    this.openLoadingDialog('Загрузка рецепта');
    this.http.createRecipeImpl(recipe, data => {
      this.saveRecipeImage(data);
    }, code => {
      this.postDialogErrorMessage(`Ошибка: ${RestUtilService.handleError(code)}`);
    }, error => {
      this.handleError(error);
    });
  }

  private handleError(error) {
    if (error.status === 401) {
      this.closeDialog();
      LoginService.userLoggedIn = false;
      this.cashCreateRecipe();
      LoginService.redirectTo = '/recipes/create';
      this.canExitFromCreate = true;
      this.http.getRouter().navigate(['auth']).then();
    } else {
      this.postDialogErrorMessage(`Ошибка: ${error}`);
    }
  }

  private saveRecipeImage(recipe: Recipe) {
    this.postDialogMessage('Загрузка изображений');
    const formData = new FormData();
    formData.append('file', this.recipeMain.loadFile);
    this.http.uploadRecipeImageImpl(recipe.recipeId, formData, res => {
      recipe.imageUrls = res;
      this.saveRecipeStepImages(recipe);
    },  errorCode => {
      this.postDialogErrorMessage(`Ошибка: ${RestUtilService.handleError(errorCode)}`);
    }, error => {
      this.handleError(error);
    });
  }
  private saveRecipeStepImages(recipe: Recipe) {
    let size = recipe.recipeSteps.length;
    const cash = this.recipeSteps.imagePathCash;
    recipe.recipeSteps.forEach( s => {
      const file = cash.get(s.number);
      const formData = new FormData();
      formData.append('file', file);
      this.http.uploadRecipeStepImageImpl(recipe.recipeId, s.recipeStepCookId, formData, data => {
        s.imageUrl = data;
        s.recipeId = recipe.recipeId;
        if (--size === 0) {
          if (recipe.ad !== null) {
            this.saveRecipeAdImage(recipe);
          } else {
            this.closeDialog();
            this.openRecipePreview(recipe);
          }
        }
      }, code => {
        this.postDialogErrorMessage(`Ошибка: ${RestUtilService.handleError(code)}`);
      }, error => {
        this.handleError(error);
      });
    });
  }
  private saveRecipeAdImage(recipe: Recipe) {
    const file = this.recipeAd.getAdImage();
    const formData = new FormData();
    formData.append('file', file);
    this.http.uploadRecipeAdImageImpl(recipe.recipeId, recipe.ad.adId, formData, data => {
      recipe.ad.imageUrl = data;
      this.closeDialog();
      this.openRecipePreview(recipe);
    }, code => {
      this.postDialogErrorMessage(`Ошибка: ${RestUtilService.handleError(code)}`);
    }, error => {
      this.handleError(error);
    });
  }
  private openRecipePreview(recipe: Recipe) {
    this.canExitFromCreate = true;
    this.cashCreateRecipe();
    this.cache.putToCache(CacheService.RECIPE_PREVIEW, 1);
    this.http.getRouter().navigate([`recipes/create/preview/${recipe.recipeId}`]).then();
  }

  isDarkTheme() {
    return AppComponent.isDarkTheme;
  }
  getFabButtonIcon() {
    const adOn = this.recipeAd ? this.recipeAd.isOn() : false;
    return this.tabIndex === 3 || (this.tabIndex === 2 && !adOn) ? 'done' : 'navigate_next';
  }

  tabIndexChanged(index) {
    this.tabIndex = index;
  }

  openLoadingDialog(msg: string): void {
    this.dialog.open(LoadingDialogComponent, {
      id: 'loading-dialog',
      width: '450px',
      data: { message: msg },
      disableClose: true
    });
  }

  canExit(): boolean {
    return this.canExitFromCreate;
  }

  buySubscription() {
    this.canExitFromCreate = true;
    this.cashCreateRecipe();
    this.cache.putToCache(CacheService.PROFILE_BUY_SUB, 1);
    this.http.getRouter().navigate(['buy_subscription']).then();
  }
}

export interface LoadingMessage {
  message: string;
}
@Component({
  selector: 'app-loading-dialog',
  templateUrl: 'loading-dialog.html',
})
export class LoadingDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: LoadingMessage) {}
}
