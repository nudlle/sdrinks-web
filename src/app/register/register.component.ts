import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../service/login.service';
import {Router} from '@angular/router';
import {ErrorMessages} from '../utils/error-messages';
import {Utils} from '../utils/utils';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  emailSub: any;
  confirmPassSub: any;
  regForm: FormGroup;
  hide = true;
  hideConfirm = true;
  email: FormControl;
  password: FormControl;
  confirmPassword: FormControl;

  private getEmail() {
    return this.login.user ? this.login.user.email : '';
  }
  private getPassword() {
    return this.login.user ? this.login.user.password : '';
  }
  constructor(private login: LoginService, private router: Router) {
  }

  initControls() {
    this.email = new FormControl(this.getEmail(),
      [Validators.required, Validators.minLength(3), Validators.maxLength(128), Validators.email]);
    this.password = new FormControl(this.getPassword(),
      [Validators.required, Validators.minLength(3), Validators.maxLength(128)]);
    this.confirmPassword = new FormControl(this.getPassword(),
      [Validators.required, Validators.minLength(3), Validators.maxLength(128)]);
  }

  initForm() {
    this.regForm = new FormGroup({
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword
    });
  }

  ngOnInit() {
    this.initControls();
    this.initForm();
    this.emailSub = this.email.valueChanges.subscribe(() => {
      if (this.password.hasError('email_already_exist')) {
        this.password.setErrors(null);
      }
    });
    this.confirmPassSub = this.confirmPassword.valueChanges.subscribe(() => {
      if (this.confirmPassword.hasError('pass_not_eq')) {
        this.confirmPassword.setErrors(null);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.emailSub) {
      this.emailSub.unsubscribe();
    }
    if (this.confirmPassSub) {
      this.confirmPassSub.unsubscribe();
    }
  }

  authRequest() {
    if (Utils.checkWhitespaces(this.email.value)) {
      this.email.setValue('');
      this.email.markAsTouched();
    } else if (Utils.checkWhitespaces(this.password.value)) {
      this.password.setValue('');
      this.password.markAsTouched();
    } else if (Utils.checkWhitespaces(this.confirmPassword.value)) {
      this.confirmPassword.setValue('');
      this.confirmPassword.markAsTouched();
    } else if (this.regForm.valid) {
      if (this.password.value !== this.confirmPassword.value) {
        this.confirmPassword.setErrors({pass_not_eq: true});
      } else {
        this.login.checkUserEmail(this.email, this.password.value);
      }
    }
  }
  goBack() {
    this.router.navigate(['auth']).then();
  }

  getErrorMsgEmail() {
    return this.email.hasError('required') ? ErrorMessages.ErrorRequired :
      this.email.hasError('email') ? ErrorMessages.ErrorEmail :
        this.email.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
          this.email.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
            this.email.hasError('email_already_exist') ? ErrorMessages.ErrorUserExists : '';
  }
  getErrorMsgPassword() {
    return this.password.hasError('required') ? ErrorMessages.ErrorRequired :
      this.password.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
        this.password.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) : '';
  }
  getErrorConfirmMsgPassword() {
    return this.confirmPassword.hasError('required') ? ErrorMessages.ErrorRequired :
      this.confirmPassword.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
        this.confirmPassword.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
          this.confirmPassword.hasError('pass_not_eq') ? ErrorMessages.errorValidate('пароли не совпадают') : '';
  }
}
