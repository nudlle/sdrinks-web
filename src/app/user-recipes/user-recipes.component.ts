import { Component, OnInit } from '@angular/core';
import {Animations} from '../animations/animations';
import {RestUtilService} from '../service/rest-util.service';
import {Recipe} from '../domain/recipe';
import {RecipeResolverService} from '../service/recipe-resolver.service';
import {ServerResponseCode} from '../response/server-response-code';
import {MatSnackBar} from '@angular/material';
import {LoginService} from '../service/login.service';
import {AppComponent} from '../app.component';
import {BindData, NavBarService} from '../service/nav-bar.service';

@Component({
  selector: 'app-user-recipes',
  templateUrl: './user-recipes.component.html',
  styleUrls: ['./user-recipes.component.scss'],
  animations: Animations.animationList
})
export class UserRecipesComponent implements OnInit {
  recipes: Array<Recipe>;
  loading = true;
  noContent = false;
  isLikedComponent: boolean;
  private removeClicked = false;

  private pushRecipe(rc: string[], r: Recipe) {
    const index = rc.indexOf(r.recipeId);
    if (index !== -1) {
      this.recipes[index] = r;
    }
  }

  constructor(private http: RestUtilService, private snackBar: MatSnackBar,
              private navBarService: NavBarService) {
    this.isLikedComponent = this.http.getRouter().url === '/liked';
    if (http.getLoginService().user) {
      const rc = this.isLikedComponent ?
        http.getLoginService().user.likedRecipes : http.getLoginService().user.ownRecipes;
      if (rc && rc.length > 0) {
        let offset = 0;
        this.recipes = new Array<Recipe>(rc.length);
        rc.forEach(rId => {
          this.http.getRecipeByIdImpl(rId, recipe => {
              this.pushRecipe(rc, recipe);
              offset++;
            },
            () => { offset++; }, error => {
              if (error.status === 401) {
                LoginService.userLoggedIn = false;
                LoginService.redirectTo = this.isLikedComponent ? '/liked' : '/added';
                this.http.getRouter().navigate(['auth']).then();
              }
            }, () => {
              this.loading = offset !== rc.length;
            });
        });
      } else {
        this.loading = false;
        this.noContent = true;
      }
    } else {
      LoginService.redirectTo = this.isLikedComponent ? '/liked' : '/added';
      this.http.getRouter().navigate(['auth']).then();
    }
  }

  getRecipeAvatarBGImage(recipe) {
    return `url(${RecipeResolverService.getRecipeImageSource(recipe)})`;
  }

  recipePostedBy(email: string) {
    return RecipeResolverService.transformUserEmail(email);
  }

  removeRecipeImpl(recipe: Recipe) {
    this.removeClicked = true;
    if (this.isLikedComponent) {
      this.removeLikedRecipe(recipe);
    } else {
      this.removeOwnRecipe(recipe);
    }
  }

  getNoContentText() {
    return this.isLikedComponent ? 'Вы не добавили ни одного рецепта в Избранное' : 'Вы не добавили ни одного рецепта';
  }

  getGoAwayText() {
    return this.isLikedComponent ? 'Перейти в Рецепты' : 'Добавить Рецепт';
  }

  getGoAwayLink() {
    return this.isLikedComponent ? '/recipes' : '/recipes/create';
  }

  private removeLikedRecipe(recipe: Recipe) {
    if (window.confirm('Рецепт будет удалён из избранного. Вы уверены?')) {
      this.http.deleteLikedRecipeImpl(this.http.getLoginService().user.userId, recipe.recipeId, code => {
        if (code === ServerResponseCode.OK_RESPONSE) {
          this.snackBar.open('Рецепт удалён из избранного', 'OK', {duration: 2000});
          this.removeRecipe(recipe);
        } else {
          this.snackBar.open(`Не удалось удалить рецепт. Ошибка: ${code}`, 'OK', {duration: 2000});
        }
      }, () => {}, () => {});
    }
  }
  private removeOwnRecipe(recipe: Recipe) {
    if (window.confirm('Рецепт будет удалён. Вы уверены?')) {
      this.http.deleteRecipeImpl(this.http.getLoginService().user.userId, recipe.recipeId, code => {
        if (code === ServerResponseCode.OK_RESPONSE) {
          this.snackBar.open('Рецепт удалён', 'OK', {duration: 2000});
          this.removeRecipe(recipe);
        } else {
          this.snackBar.open(`Не удалось удалить рецепт. Ошибка: ${code}`, 'OK', {duration: 2000});
        }
      }, () => {}, () => {});
    }
  }
  private removeRecipe(recipe: Recipe) {
    let index = this.recipes.indexOf(recipe);
    if (index !== -1) {
      this.recipes.splice(index, 1);
    }
    index = this.http.getLoginService().user.likedRecipes.indexOf(recipe.recipeId);
    if (index !== -1) {
      this.http.getLoginService().user.likedRecipes.splice(index, 1);
    }
    this.noContent = this.recipes.length === 0;
  }

  openRecipe(recipe: Recipe) {
    if (!this.removeClicked) {
      this.navBarService.emitEvent(BindData.HideSearch);
      this.http.getRouter().navigate([`recipes/show/${recipe.recipeId}`]).then();
    } else {
      this.removeClicked = false;
    }
  }

  ngOnInit() {
  }

  isDarkTheme() {
    return AppComponent.isDarkTheme;
  }

}
