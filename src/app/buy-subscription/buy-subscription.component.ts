import {Component, OnInit} from '@angular/core';
import {CacheService} from '../service/cache.service';
import {CanExitGuard} from '../create-recipe/create-recipe.component';
import {LoginService} from '../service/login.service';
import {RestUtilService} from '../service/rest-util.service';
import {RecipeResolverService} from '../service/recipe-resolver.service';

@Component({
  selector: 'app-buy-subscription',
  templateUrl: './buy-subscription.component.html',
  styleUrls: ['./buy-subscription.component.scss']
})
export class BuySubscriptionComponent implements OnInit, CanExitGuard {
  exitFromCreateRecipe = true;
  constructor(private http: RestUtilService, private cache: CacheService) {
    this.exitFromCreateRecipe = cache.getFromCache(CacheService.PROFILE_BUY_SUB) === 1;
    if (http.getLoginService().user) {
      const sub = http.getLoginService().user.userSubscription;
      if (sub && RecipeResolverService.checkSub(http.getLoginService().user)) {
        this.http.getRouter().navigate(['profile']).then();
      }
    } else {
      LoginService.redirectTo = '/buy_subscription';
      this.http.getRouter().navigate(['auth']).then();
    }
  }

  ngOnInit() {
    this.cache.putToCache(CacheService.PROFILE_BUY_SUB, 0);
  }

  canExit(): boolean {
    return !this.exitFromCreateRecipe;
  }
}
