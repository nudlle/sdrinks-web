import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RestUtilService} from '../service/rest-util.service';
import {environment} from '../../environments/environment';
import {ErrorMessages} from '../utils/error-messages';
import {Utils} from '../utils/utils';

@Component({
  selector: 'app-bottom-bar',
  templateUrl: './bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.scss']
})
export class BottomBarComponent implements OnInit {
  usageRestrictions = '';
  messageToSend = '';
  constructor(private dialog: MatDialog, private http: RestUtilService, private snackbar: MatSnackBar) {
    http.getUsageRestrictionsImpl(data => this.usageRestrictions = data,
      code => this.usageRestrictions = `Ошибка загрузки: ${code}`);
  }

  private sendMessage() {
    const from = (this.http.getLoginService().user && this.http.getLoginService().user.userId) ?
      `${this.http.getLoginService().user.userId}` : 'user_undefined';
    const userMessage = {
      userMessageId: null,
      userId: from,
      message: this.messageToSend,
      appVersion: environment.version,
      date: null
    };
    this.http.postUserMessageImpl(userMessage, um => {
      this.snackbar.open(`Сообщение #${um.userMessageId} зарегестрированно`, 'Ok', {duration: 2000});
    }, errorCode => {
      this.snackbar.open(`Не удалось отправить сообщение: ${errorCode}`, 'Ok', {duration: 2000});
    });
  }
  ngOnInit() {
  }

  openSendUsDialog(): void {
    const dialogRef = this.dialog.open(SendToUsDialogComponent, {
      width: '90vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.messageToSend = result;
        this.sendMessage();
      }
    });
  }
  openUsageRestriction(): void {
    this.dialog.open(UsageRestrictionDialogComponent, {
      width: '90vw',
      height: '85vh',
      data: { restrictions: this.usageRestrictions }
    });
  }
}

@Component({
  selector: 'app-send-to-us-dialog',
  templateUrl: 'send-to-us-dialog.html',
})
export class SendToUsDialogComponent implements OnInit {
  sendMessageGroup: FormGroup;
  message: FormControl;

  initGroup() {
    this.sendMessageGroup = new FormGroup({
      message: this.message
    });
  }
  initFormControl() {
    this.message = new FormControl('', [Validators.required, Validators.maxLength(1500)]);
  }

  constructor(public dialogRef: MatDialogRef<SendToUsDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.initFormControl();
    this.initGroup();
  }

  getErrorMessage() {
    return this.message.hasError('required') ? ErrorMessages.ErrorRequired :
            this.message.hasError('maxlength') ? ErrorMessages.errorMaxLength(1500) : '';
  }

  check() {
    return Utils.checkWhitespaces(this.message.value);
  }

  sendMessage() {
    if (this.check()) {
      this.message.setValue('');
      this.message.markAsTouched();
    } else if (this.message.value.length === 0) {
      this.message.markAsTouched();
    } else {
      this.dialogRef.close(this.message.value);
    }
  }
}
interface UsageRestrictions {
  restrictions: string;
}
@Component({
  selector: 'app-usage-restriction-dialog',
  templateUrl: 'usage-restriction-dialog.html',
})
export class UsageRestrictionDialogComponent {
  constructor(public dialogRef: MatDialogRef<UsageRestrictionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UsageRestrictions) {}
  onClick(): void {
    this.dialogRef.close();
  }
}
