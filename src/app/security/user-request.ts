import { User } from '../domain/user';

export interface UserRequest {
    user: User;
    timestamp: number;
}
