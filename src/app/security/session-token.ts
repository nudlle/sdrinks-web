export interface SessionToken {
    id: string;
    token: string;
    timestamp: number;
}
