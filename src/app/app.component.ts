import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {RestUtilService} from './service/rest-util.service';
import {LoginService} from './service/login.service';
import {OverlayContainer} from '@angular/cdk/overlay';
import {BindData, NavBarService} from './service/nav-bar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public static isDarkTheme = false;
  constructor(private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private http: RestUtilService,
              private navBarService: NavBarService,
              private overlayContainer: OverlayContainer) {
    this.iconsRegistry();
    const userId = localStorage.getItem('userId');
    if (userId) {
      this.http.getUserImpl(userId, user => {
        http.getLoginService().user = user;
        LoginService.userLoggedIn = true;
      }, () => {
        LoginService.userLoggedIn = false;
      }, () => {}, () => {});
    } else {
      LoginService.userLoggedIn = false;
    }
  }
  private overlayContainerTheme() {
    const themeClass = AppComponent.isDarkTheme ? 'dark-theme' : 'light-theme';

    // remove old theme class and add new theme class
    // we're removing any css class that contains '-theme' string but your theme classes can follow any pattern
    const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
    const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('-theme'));
    if (themeClassesToRemove.length) {
      overlayContainerClasses.remove(...themeClassesToRemove);
    }
    overlayContainerClasses.add(themeClass);
  }
  private darkThemeListener = (e: any) => {
    if (e.matches) {
      AppComponent.isDarkTheme = true;
      this.overlayContainerTheme();
    }
  }
  private lightThemeListener = (e: any) => {
    if (e.matches) {
      AppComponent.isDarkTheme = false;
      this.overlayContainerTheme();
    }
  }

  ngOnInit(): void {
    const isDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
    const isLightMode = window.matchMedia('(prefers-color-scheme: light)').matches;
    const isNotSpecified = window.matchMedia('(prefers-color-scheme: no-preference)').matches;
    const hasNoSupport = !isDarkMode && !isLightMode && !isNotSpecified;

    window.matchMedia('(prefers-color-scheme: dark)').addListener(this.darkThemeListener);
    window.matchMedia('(prefers-color-scheme: light)').addListener(this.lightThemeListener);
    // const dark = window.matchMedia('(prefers-color-scheme: dark)');
    // dark.addEventListener('change', this.darkThemeListener);
    // const light = window.matchMedia('(prefers-color-scheme: light)');
    // light.addEventListener('change', this.lightThemeListener);

    if (isDarkMode) { AppComponent.isDarkTheme = true; }
    if (isLightMode) { AppComponent.isDarkTheme = false; }
    if (isNotSpecified || hasNoSupport) {
      const now = new Date();
      const hour = now.getHours();
      if (hour < 4 || hour >= 16) {
        AppComponent.isDarkTheme = true;
      }
    }
    // AppComponent.isDarkTheme = false;

    this.overlayContainerTheme();
  }

  ngOnDestroy(): void {
    window.matchMedia('(prefers-color-scheme: dark)').removeListener(this.darkThemeListener);
    window.matchMedia('(prefers-color-scheme: light)').removeListener(this.lightThemeListener);
    // window.matchMedia('(prefers-color-scheme: dark)').removeEventListener('change', this.darkThemeListener);
    // window.matchMedia('(prefers-color-scheme: light)').removeEventListener('change', this.lightThemeListener);
  }

  isRouteRecipesOrProfile() {
    return this.http.getRouter().isActive('recipes', true) ||
      this.http.getRouter().isActive('profile', true);
  }
  isDarkThemeActivated() {
    return AppComponent.isDarkTheme;
  }
  iconsRegistry() {
    this.matIconRegistry.addSvgIcon('ic_alco_drink',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_alco_drink.svg'));
    this.matIconRegistry.addSvgIcon('ic_berries_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_berries.svg'));
    this.matIconRegistry.addSvgIcon('ic_berries_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_berries_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_cocktail_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_cocktail.svg'));
    this.matIconRegistry.addSvgIcon('ic_cocktail_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_cocktail_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_cold_drink_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_cold_drink.svg'));
    this.matIconRegistry.addSvgIcon('ic_cold_drink_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_cold_drink_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_fruit_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_fruit.svg'));
    this.matIconRegistry.addSvgIcon('ic_fruit_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_fruit_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_herb_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_herb.svg'));
    this.matIconRegistry.addSvgIcon('ic_herb_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_herb_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_hot_drink_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_hot_drink.svg'));
    this.matIconRegistry.addSvgIcon('ic_hot_drink_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_hot_drink_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_lemonade_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_lemonade.svg'));
    this.matIconRegistry.addSvgIcon('ic_lemonade_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_lemonade_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_liqueur_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_liqueur.svg'));
    this.matIconRegistry.addSvgIcon('ic_liqueur_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_liqueur_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_milkshake_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_milkshake.svg'));
    this.matIconRegistry.addSvgIcon('ic_milkshake_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_milkshake_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_shot_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_shot.svg'));
    this.matIconRegistry.addSvgIcon('ic_shot_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_shot_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_sour_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_sour.svg'));
    this.matIconRegistry.addSvgIcon('ic_sour_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_sour_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_spicy_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_spicy.svg'));
    this.matIconRegistry.addSvgIcon('ic_spicy_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_spicy_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_sweet_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_sweet.svg'));
    this.matIconRegistry.addSvgIcon('ic_sweet_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_sweet_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_tea_coffee_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_tea_coffee.svg'));
    this.matIconRegistry.addSvgIcon('ic_tea_coffee_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_tea_coffee_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_vegetables_dark',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_vegetables.svg'));
    this.matIconRegistry.addSvgIcon('ic_vegetables_light',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/ic_vegetables_black.svg'));
    this.matIconRegistry.addSvgIcon('ic_app_logo',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/app_logo.svg'));
  }

  addNewRecipe() {
    if (LoginService.userLoggedIn) {
      this.http.getRouter().navigate(['/recipes/create']).then();
    } else {
      LoginService.redirectTo = '/recipes/create';
      this.http.getRouter().navigate(['auth']).then();
    }
  }
}
