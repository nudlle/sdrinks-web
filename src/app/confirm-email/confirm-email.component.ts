import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../service/login.service';
import {Router} from '@angular/router';
import {ErrorMessages} from '../utils/error-messages';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit, OnDestroy {
  showSend = false;
  confirmEmailSub: any;
  confirmEmailForm: FormGroup;
  confirmEmail: FormControl;
  constructor(private login: LoginService, private router: Router) {
    if (!login.user) {
      router.navigate(['auth']).then();
    }
  }

  initControls() {
    this.confirmEmail = new FormControl('', [Validators.required]);
  }

  initForm() {
    this.confirmEmailForm = new FormGroup({
      confirmEmail: this.confirmEmail
    });
  }

  ngOnInit() {
    this.initControls();
    this.initForm();
    this.confirmEmailSub = this.confirmEmail.valueChanges.subscribe(() => {
      if (this.confirmEmail.hasError('bad_check_code')) {
        this.confirmEmail.setErrors(null);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.confirmEmailSub) {
      this.confirmEmailSub.unsubscribe();
    }
  }

  checkEmailAndRegister() {
    if (this.confirmEmailForm.valid) {
      this.login.registerImpl(this.confirmEmail, show => this.showSend = show);
    }
  }

  sendAgainConfirmEmail() {
    this.login.reSendCheckUserEmail(this.confirmEmail, show => this.showSend = show);
  }

  goBack() {
    this.router.navigate(['auth/register']).then();
  }
  getErrorMsgCheckEmail() {
    return this.confirmEmail.hasError('required') ? ErrorMessages.ErrorRequired :
            this.confirmEmail.hasError('bad_check_code') ? ErrorMessages.errorValidate('не верный код') : '';
  }
}
