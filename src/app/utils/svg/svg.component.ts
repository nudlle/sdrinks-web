import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.scss']
})
export class SvgComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @Input() options = {
    type: 'left',
    strokeOpacity: 1,
    strokeLinecap: 'round',
    strokeWidth: 10,
    strokeDashArray: '0'
  };
  @Input() stroke = AppComponent.isDarkTheme ? '#fde82f' : '#303f9f';
  startX = 0;
  startY = 0;
  moveX = 0;
  moveY = 0;
  isPatching = true;

  @ViewChild('svgContainer', {static: true}) container: ElementRef;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    if (this.container) {
      const width = this.container.nativeElement.offsetWidth;
      const height = this.container.nativeElement.offsetHeight;
      if (this.startY === height / 2) { return; }
      this.startY = height / 2;
      this.moveX = width / 2;
      this.moveY = height - 20;
      switch (this.options.type) {
        case 'left': {
          this.startX = 8;
          break;
        }
        case 'right': {
          this.startX = width - 8;
          break;
        }
      }
      this.cdr.detectChanges();
    }
  }

  ngAfterViewInit(): void {
  }

  ngOnInit() {
  }
  public drawLines() {
  }

  public startPatch(el: ElementRef, e: MouseEvent) {
    this.startX = el.nativeElement.offsetLeft
      + (el.nativeElement.offsetWidth / 2);

    this.startY = el.nativeElement.offsetTop
      + (el.nativeElement.offsetHeight / 2);

    this.moveX = e.clientX;
    this.moveY = e.clientY;

    this.isPatching = true;
  }

  public movePatch(mm: MouseEvent) {
    this.moveX = mm.clientX;
    this.moveY = mm.clientY;
  }

  public endPatch() {
    this.isPatching = false;
  }

  get cX1() {
    return this.startX;
  }

  get cY1() {
    return this.startY;
  }

  get cX2() {
    return this.moveX;
  }

  get cY2() {
    return this.moveY;
  }

  get path() {
    const x1 = this.startX;
    const y1 = this.startY;

    const pY = y1;
    let pX = 0;
    switch (this.options.type) {
      case 'left': {
        pX = this.moveX + ((this.moveX / 4) + (this.moveX / 2));
        break;
      }
      case 'right': {
        pX = x1 - this.moveX - ((this.moveX / 4) + (this.moveX / 2));
      }
    }
    return `M ${x1} ${y1} Q ${pX} ${pY}, ${this.moveX} ${this.moveY}`;
  }
}
