export class ErrorMessages {
  public static ErrorRequired = 'Поле должно быть заполнено';
  public static ErrorEmail = 'Не верный эл. адрес';
  public static ErrorUserNotFound = 'Пользователь не найден';
  public static ErrorUserExists = 'Пользователь уже существует';
  public static ErrorUrl = 'Не верный адрес ссылки';

  public static errorMinLength(length: number) {
    return `Минимальное значения поля ${length} символов`;
  }
  public static errorMaxLength(length: number) {
    return `Максимальное значение поля ${length} символов`;
  }
  public static errorValidate(msg?: string) {
    return `Ошибка проверки${msg ? ` : ${msg}` : ''}`;
  }
}
