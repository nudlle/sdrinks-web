
export class Utils {
  public static checkWhitespaces(text: string): boolean {
    return text.trim().length === 0;
  }
}
