import { DataResponse } from './data-response';
import { Recipe } from '../domain/recipe';

export interface RecipeResponse extends DataResponse<Recipe> {
}
