import { DataResponse } from './data-response';
import { Subscription } from '../domain/subscription';

export interface SubscriptionResponse extends DataResponse<Subscription> {
}
