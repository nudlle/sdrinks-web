import { DataResponse } from './data-response';
import { User } from '../domain/user';

export interface UserResponse extends DataResponse<User> {
}
