import { DataResponse } from './data-response';

export interface StringResponse extends DataResponse<string> {
}
