import { DataResponse } from './data-response';
import { UserComment } from '../domain/user-comment';

export interface CommentResponse extends DataResponse<UserComment> {
}
