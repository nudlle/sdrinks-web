import { DataResponse } from './data-response';
import { SessionToken } from '../security/session-token';

export interface SessionTokenResponse extends DataResponse<SessionToken> {
}
