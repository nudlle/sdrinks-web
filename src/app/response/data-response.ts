import { ServerResponseCode } from './server-response-code';

export interface DataResponse<T> {
    data: T;
    code: ServerResponseCode;
}
