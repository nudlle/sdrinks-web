import { DataResponse } from './data-response';
import { UserMessage } from '../domain/user-message';

export interface UserMessageResponse extends DataResponse<UserMessage> {
}
