import { DataResponse } from './data-response';
import { ImageUrls } from '../domain/image-urls';

export interface ImageUrlsResponse extends DataResponse<ImageUrls> {
}
