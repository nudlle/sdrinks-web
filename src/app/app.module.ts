import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NavBarComponent, SearchParamsBottomSheetComponent} from './nav-bar/nav-bar.component';
import {BottomBarComponent, SendToUsDialogComponent, UsageRestrictionDialogComponent} from './bottom-bar/bottom-bar.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatRadioModule } from '@angular/material/radio';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipesComponent } from './recipes/recipes.component';
import {CommentRecipeDialogComponent, RecipeComponent} from './recipe/recipe.component';
import { ProfileComponent } from './profile/profile.component';
import { AboutComponent } from './about/about.component';
import {CreateRecipeComponent, LoadingDialogComponent} from './create-recipe/create-recipe.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {
  MAT_BOTTOM_SHEET_DEFAULT_OPTIONS,
  MAT_DIALOG_DEFAULT_OPTIONS, MatAutocompleteModule, MatBottomSheetModule, MatButtonToggleModule, MatCheckboxModule,
  MatDialogModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSnackBarModule, MatTabsModule, MatTooltipModule
} from '@angular/material';
import {LoginService} from './service/login.service';
import {RestServiceService} from './service/rest-service.service';
import {RestUtilService} from './service/rest-util.service';
import {LoggingInterceptor} from './interceptors/logging-interceptor';
import { RecoverComponent } from './recover/recover.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import {AuthGuard} from './guard/auth.guard';
import {ProfileGuard} from './guard/profile.guard';
import {MatCardModule} from '@angular/material/card';
import { UserRecipesComponent } from './user-recipes/user-recipes.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {RecipesService} from './service/recipes.service';
import {CacheService} from './service/cache.service';
import { SvgComponent } from './utils/svg/svg.component';
import { CommentsComponent } from './recipe/comments/comments.component';
import localeRu from '@angular/common/locales/ru';
import {registerLocaleData} from '@angular/common';
import {NavBarService} from './service/nav-bar.service';
import { RecipeMainComponent } from './create-recipe/recipe-main/recipe-main.component';
import { RecipeIngredientsComponent } from './create-recipe/recipe-ingredients/recipe-ingredients.component';
import { RecipeStepsComponent } from './create-recipe/recipe-steps/recipe-steps.component';
import { RecipeAdComponent } from './create-recipe/recipe-ad/recipe-ad.component';
import {ExitCreateRecipeGuard} from './guard/exit-create-recipe.guard';
import {ExitPreviewRecipeGuard} from './guard/exit-preview-recipe.guard';
import { BuySubscriptionComponent } from './buy-subscription/buy-subscription.component';
import { SearchResultComponent } from './search-result/search-result.component';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
export const dialogProviders = [
  { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true }}
];
export const bottomSheetProvides = [
  { provide: MAT_BOTTOM_SHEET_DEFAULT_OPTIONS, useValue: { hasBackdrop: true }}
];

export const localeProviders = [{provide: LOCALE_ID, useValue: 'ru'}];

registerLocaleData(localeRu, 'ru');

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    BottomBarComponent,
    RecipesComponent,
    RecipeComponent,
    ProfileComponent,
    AboutComponent,
    CreateRecipeComponent,
    MainComponent,
    LoginComponent,
    RegisterComponent,
    RecoverComponent,
    ConfirmEmailComponent,
    UserRecipesComponent,
    SendToUsDialogComponent,
    UsageRestrictionDialogComponent,
    CommentRecipeDialogComponent,
    SvgComponent,
    CommentsComponent,
    RecipeMainComponent,
    RecipeIngredientsComponent,
    RecipeStepsComponent,
    RecipeAdComponent,
    LoadingDialogComponent,
    BuySubscriptionComponent,
    SearchResultComponent,
    SearchParamsBottomSheetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatRadioModule,
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatRippleModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    MatCardModule,
    MatSnackBarModule,
    ScrollingModule,
    MatDialogModule,
    MatTooltipModule,
    MatTabsModule,
    MatCheckboxModule,
    MatButtonToggleModule,
    MatBottomSheetModule,
    MatAutocompleteModule
  ],
  entryComponents: [SendToUsDialogComponent, UsageRestrictionDialogComponent,
    CommentRecipeDialogComponent, LoadingDialogComponent, SearchParamsBottomSheetComponent],
  providers: [LoginService, RestServiceService, RestUtilService, RecipesService, CacheService, NavBarService,
    AuthGuard, ProfileGuard, httpInterceptorProviders, dialogProviders, localeProviders, ExitCreateRecipeGuard,
    ExitPreviewRecipeGuard, bottomSheetProvides],
  bootstrap: [AppComponent]
})
export class AppModule {
}
