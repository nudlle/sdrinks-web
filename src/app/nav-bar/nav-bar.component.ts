import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {LoginService} from '../service/login.service';
import {fromEvent, interval, Observable, Subscription} from 'rxjs';
import {distinctUntilChanged, filter, map, pairwise, share, startWith, take, tap, throttleTime} from 'rxjs/operators';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef, MatDialogRef, MatSidenav} from '@angular/material';
import {CacheService} from '../service/cache.service';
import {NavigationEnd, Router} from '@angular/router';
import {AppComponent} from '../app.component';
import {BindData, NavBarService} from '../service/nav-bar.service';
import {RecipeToSearch} from '../domain/recipe-to-search';
import {DrinkState} from '../domain/drink-state';
import {DrinkType} from '../domain/drink-type';
import {DrinkTaste} from '../domain/drink-taste';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessages} from '../utils/error-messages';
import {Utils} from '../utils/utils';
import {RecipeResolverService} from '../service/recipe-resolver.service';
import {SearchResultComponent} from '../search-result/search-result.component';

enum Direction {
  UP, DOWN
}
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
  animations: []
})
export class NavBarComponent implements OnInit, AfterViewInit, OnDestroy {
  searchOptions = [
    'Лимонад', 'Сок', 'Молоко', 'Ягоды', 'Кофе', 'Чай', 'Маргарита', 'Фенистил', 'Какахи', 'Моджо Поджо', 'Сахар', 'Мёд'
  ];
  searchIndex = '';
  searchActivated = false;
  isVisible = true;
  recipeToSearch: RecipeToSearchCustomize = {
    search: '',
    tastes: [],
    type: 'Unspecified',
    state: 'Unspecified',
    alco: '-1'
  };
  private scrollUpSub: Subscription = null;
  private scrollDownSub: Subscription = null;
  private routerNavSub: Subscription = null;
  private activeRoute: string = null;
  private scrollTopSub: Subscription = null;
  private filteredSearchOptions: Observable<string[]>;
  private searchControl = new FormControl();

  private static unsubscribe(sub: Subscription) {
    if (sub) {
      sub.unsubscribe();
    }
  }

  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.searchOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  getToolbarHeight() {
    return document.querySelector('.mat-toolbar').clientHeight;
  }

  getToolbarHeightCss() {
    return `-${document.querySelector('.mat-toolbar').clientHeight}px`;
  }

  calcMarginTop() {
    const v = document.querySelector('.mat-toolbar').clientHeight;
    if (v) {
      return `${v + 6}px`;
    }
    return '70px';
  }

  calcMarginBottom() {
    const v = document.querySelector('.bottom-bar').clientHeight;
    if (v) {
      return `${v + 6}px`;
    }
    return '70px';
  }

  getBottomBarHeight() {
    return document.querySelector('app-bottom-bar').clientHeight;
  }

  hideBottomBar() {
    return '0';
    // return `-${document.querySelector('app-bottom-bar').clientHeight}px`;
  }

  checkSideNavClick(sideNav: MatSidenav) {
    if (sideNav.opened) {
      sideNav.toggle().then(() => {});
    }
  }

  private restoreScroll(url: string) {
    const scrollTop = this.cache.getFromCache(url);
    if (scrollTop) {
      const options: ScrollToOptions = {
        left: 0,
        top: scrollTop,
        behavior: 'smooth'
      };
      interval(200).pipe(
        take(1)
      ).subscribe(() => {
          document.querySelector('.mat-sidenav-content').scrollTo(options);
        });
    }
    this.activeRoute = url;
  }

  ngAfterViewInit() {
    const content = document.querySelector('.mat-sidenav-content');
    const scroll$ = fromEvent(content, 'scroll').pipe(
      throttleTime(10),
      map(() => content.scrollTop),
      tap(st => this.cache.putToCache(this.activeRoute, st)),
      pairwise(),
      map(([y1, y2]): Direction => {
        if (y1 <= 0 || y2 < 0) {
          return Direction.UP;
        }
        if (y1 >= document.body.offsetHeight || y2 >= document.body.offsetHeight) {
          return Direction.DOWN;
        }
        return y2 < y1 ? Direction.UP : Direction.DOWN;
      }),
      distinctUntilChanged(),
      share()
    );
    const scrollUp$ = scroll$.pipe(
      filter(direction => direction === Direction.UP)
    );

    const scrollDown$ = scroll$.pipe(
      filter(direction => direction === Direction.DOWN)
    );
    this.scrollUpSub = scrollUp$.subscribe(() => {
      this.isVisible = true;
    });
    this.scrollDownSub = scrollDown$.subscribe(() => {
      this.isVisible = false;
    });
    this.routerNavSub = this.router.events.pipe(
      filter((e: any): e is NavigationEnd => e instanceof NavigationEnd),
      tap(ne => this.restoreScroll(ne.url))
    ).subscribe();
    this.scrollTopSub = this.navBarService.subscribe(data => {
      if (data === BindData.ScrollTop) {
         content.scrollTo(0, 0);
      } else if (data === BindData.HideSearch) {
        this.searchActivated = false;
      }
    });
  }

  constructor(private router: Router,
              private cache: CacheService,
              private navBarService: NavBarService,
              private bottomSheet: MatBottomSheet) {}

  ngOnInit() {
    this.filteredSearchOptions = this.searchControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filter(value))
      );
  }

  ngOnDestroy(): void {
    NavBarComponent.unsubscribe(this.scrollUpSub);
    NavBarComponent.unsubscribe(this.scrollDownSub);
    NavBarComponent.unsubscribe(this.routerNavSub);
    NavBarComponent.unsubscribe(this.scrollTopSub);
  }

  isUserLoggedIn() {
    return LoginService.userLoggedIn;
  }

  openSearchParams() {
    const ref = this.bottomSheet.open(SearchParamsBottomSheetComponent, { data: this.recipeToSearch, disableClose: true });
    ref.afterDismissed().subscribe(rs => {
      this.recipeToSearch = rs;
    });
  }

  private getTextIndex(): string {
    let index = '';
    if (this.recipeToSearch.state !== 'Unspecified') {
      index += `${this.recipeToSearch.state} `;
    }
    if (this.recipeToSearch.type !== 'Unspecified') {
      index += `${this.recipeToSearch.type} `;
    }
    this.recipeToSearch.tastes.forEach((t, i) => { if (t) { index += `${RecipeResolverService.getDrinkTaste(i)} `; } });
    if (this.recipeToSearch.alco === '1') {
      index += 'Alco ';
    }
    index += this.searchIndex;
    if (index === '') {
      index = 'none';
    }
    return index;
  }
  openSearchRecipes() {
    const dd: RecipeToSearch = {
      search: this.getTextIndex(),
      state: null,
      type: null,
      tastes: null,
      alcohol: null,
    };
    this.cache.putSearchCache(dd);
    if (this.router.url === '/search-result') {
      this.navBarService.refreshSearchEvent(dd);
    } else {
      this.navBarService.lastUrlBeforeSearch = this.router.url;
      this.router.navigate(['search-result']).then();
    }
  }

  search() {
    if (this.searchActivated) {
      this.openSearchRecipes();
    } else {
      this.searchActivated = true;
    }
  }
  getSearchThemeIcon() {
    return this.isDarkThemeActivated() ? '../../assets/ic_customize_search_black.svg' : '../../assets/ic_customize_search.svg';
  }
  isDarkThemeActivated() {
    return AppComponent.isDarkTheme;
  }
  isRecipeActivated() {
    return this.navBarService.isRecipeActivated();
  }
  isRecipeCommentActivated() {
    return this.navBarService.isCommentActivated();
  }
  getRecipeActivatedName() {
    return this.navBarService.getRecipeNames();
  }
  goBackFromCommentsToRecipe() {
    this.navBarService.emitEvent(BindData.HideComments);
  }
}

interface RecipeToSearchCustomize {
  search: string;
  state: string;
  type: string;
  tastes: string[];
  alco: string;
}
@Component({
  selector: 'app-search-params-bottom-sheet',
  templateUrl: 'search-params-bottom-sheet.html',
  styleUrls: ['search-params-bottom-sheet.scss']
})
export class SearchParamsBottomSheetComponent implements OnDestroy {
  sub: Subscription;
  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: RecipeToSearchCustomize,
              private bottomSheetRef: MatBottomSheetRef<SearchParamsBottomSheetComponent>) {
    this.sub = this.bottomSheetRef.backdropClick().subscribe(m => {
      this.bottomSheetRef.dismiss(data);
    });
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
