import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Recipe} from '../domain/recipe';
import {RestUtilService} from '../service/rest-util.service';
import {DrinkTaste} from '../domain/drink-taste';
import {DrinkType} from '../domain/drink-type';
import {DrinkState} from '../domain/drink-state';
import {RecipeImageType, RecipeResolverService} from '../service/recipe-resolver.service';
import {Animations} from '../animations/animations';
import {RecipesService} from '../service/recipes.service';
import {Subscription} from 'rxjs';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/overlay';
import {BindData, NavBarService} from '../service/nav-bar.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
  animations: Animations.animationList
})
export class RecipesComponent implements OnInit, AfterViewInit, OnDestroy {
  loading = true;
  loadingNextPage = false;
  scrollSub: Subscription = null;

  constructor(private httpUtil: RestUtilService,
              private recipeService: RecipesService,
              private scrolling: ScrollDispatcher,
              private navBarService: NavBarService) {
    if (this.recipeService.allRecipes.length === 0) {
      this.recipeService.loadNextPage(() => this.loading = false, () => { this.loadingNextPage = false; }, () => {
        this.loading = false;
      });
    } else {
      this.loading = false;
    }
  }

  private windowScrolling(data: CdkScrollable) {
    const scrollTop = data.getElementRef().nativeElement.scrollTop;
    const offsetHeight = data.getElementRef().nativeElement.offsetHeight;
    const scrollHeight = data.getElementRef().nativeElement.scrollHeight;
    if (scrollTop + offsetHeight >= scrollHeight && !this.loadingNextPage) {
      this.loadingNextPage = true;
      if (this.recipeService.doLoad()) {
        this.recipeService.loadNextPage(() => this.loadingNextPage = false, () => { this.loadingNextPage = false; }, () => {
          this.loadingNextPage = false;
        });
      } else {
        this.loadingNextPage = false;
      }
    }
  }

  ngAfterViewInit(): void {
    this.scrollSub = this.scrolling.scrolled().subscribe((data: CdkScrollable) => {
      this.windowScrolling(data);
    });
  }

  getRecipes() {
    return this.recipeService.allRecipes;
  }

  getRecipeTaste(taste: DrinkTaste) {
    return RecipeResolverService.getRecipeTaste(taste);
  }

  getRecipeType(type: DrinkType) {
    return RecipeResolverService.getRecipeType(type);
  }

  getRecipeState(state: DrinkState) {
    return RecipeResolverService.getRecipeState(state);
  }

  transformEmail(email: string) {
    return RecipeResolverService.transformUserEmail(email);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.scrollSub) {
      this.scrollSub.unsubscribe();
    }
  }

  getImageSource(recipe: Recipe) {
    return `url(${RecipeResolverService.getRecipeImageSource(recipe, RecipeImageType.MEDIUM)})`;
  }

  openRecipe(index: number, recipe: Recipe) {
    // this.navBarService.emitEvent(BindData.HideSearch);
    // const usr = this.httpUtil.getLoginService().user;
    // if (usr && usr.userId !== recipe.userId) {
    //   this.httpUtil.incRecipeViewedImpl(recipe.recipeId, () => {});
    // } else {
    //   this.httpUtil.incRecipeViewedImpl(recipe.recipeId, () => {});
    // }
    this.recipeService.openRecipe(index, recipe);
  }
}
