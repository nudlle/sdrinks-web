import { Injectable } from '@angular/core';
import {RecipeCreateCache} from '../create-recipe/create-recipe.component';
import {RecipeToSearch} from '../domain/recipe-to-search';
import {DrinkState} from '../domain/drink-state';
import {DrinkType} from '../domain/drink-type';
import {DrinkTaste} from '../domain/drink-taste';


@Injectable({
  providedIn: 'root'
})
export class CacheService {
  public static RECIPE_PREVIEW = 'preview';
  public static PROFILE_BUY_SUB = 'profile_buy_subscription';
  private map = new Map<string, number>();
  private recipeCreateCache: RecipeCreateCache;
  private recipeToSearch: RecipeToSearch = {
    search: '',
    state: DrinkState.Hot,
    type: DrinkType.Lemonade,
    tastes: [],
    alcohol: false
  };
  constructor() { }
  putToCache(key: string, scrollTop: number) {
    this.map.set(key, scrollTop);
  }
  getFromCache(key: string) {
    return this.map.get(key);
  }
  putToCacheR(recipeCache: RecipeCreateCache) {
    this.recipeCreateCache = recipeCache;
  }
  getFromCacheR() {
    return this.recipeCreateCache;
  }
  putSearchCache(search: RecipeToSearch) {
    this.recipeToSearch = search;
  }
  getSearchCache() {
    return this.recipeToSearch;
  }
}
