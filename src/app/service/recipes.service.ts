import { Injectable } from '@angular/core';
import {RestUtilService} from './rest-util.service';
import {Recipe} from '../domain/recipe';
import {ServerResponseCode} from '../response/server-response-code';
import {RecipeToSearch} from '../domain/recipe-to-search';
import {BindData, NavBarService} from './nav-bar.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  allRecipes: Recipe[] = [];
  searchedRecipes: Recipe[] = [];
  recipesPageSize = 120;
  currentActiveIndex = -1;
  recipeSearchPageSize = 120;

  constructor(private http: RestUtilService, private navBarService: NavBarService) {}

  doLoad() {
    return this.allRecipes.length % this.recipesPageSize === 0;
  }

  doLoadSearch() {
    return this.searchedRecipes.length % this.recipeSearchPageSize === 0;
  }

  getNextPage() {
    const dd = this.allRecipes.length % this.recipesPageSize;
    if (dd === 0) {
      return this.allRecipes.length / this.recipesPageSize;
    }
    return 0;
  }

  getNextSearchPage() {
    const dd = this.searchedRecipes.length % this.recipeSearchPageSize;
    if (dd === 0) {
      return this.searchedRecipes.length / this.recipeSearchPageSize;
    }
    return 0;
  }

  loadNextPage(errorCode: (code: ServerResponseCode) => void,
               exception: (error: Error) => void,
               complete: () => void) {
    this.http.getRecipesImpl(this.getNextPage(), this.recipesPageSize, data => {
      this.allRecipes.push(data);
    }, errorCode, exception, complete);
  }

  loadNextSearchPage(searchRecipe: RecipeToSearch,
                     errorCode: (code: ServerResponseCode) => void,
                     exception: (error: Error) => void,
                     complete: () => void) {
    this.http.searchRecipesImpl(searchRecipe, this.getNextSearchPage(), this.recipeSearchPageSize, data => {
      this.searchedRecipes.push(data);
    }, errorCode, exception, complete);
  }

  openRecipe(index: number, recipe: Recipe) {
    this.navBarService.emitEvent(BindData.HideSearch);
    const usr = this.http.getLoginService().user;
    if (usr) {
      if (usr.userId !== recipe.userId) {
        this.http.incRecipeViewedImpl(recipe.recipeId, () => {});
      }
    } else {
      this.http.incRecipeViewedImpl(recipe.recipeId, () => {});
    }
    this.currentActiveIndex = index;
    this.http.getRouter().navigate([`recipes/show/${recipe.recipeId}`]).then();
  }

  updateRecipe(recipe: Recipe) {
    if (this.currentActiveIndex !== -1) {
      this.allRecipes[this.currentActiveIndex] = recipe;
    }
  }
}
