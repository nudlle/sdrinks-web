import {ChangeDetectorRef, Injectable} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Recipe} from '../domain/recipe';
import {Router} from '@angular/router';
import {RecipeToSearch} from '../domain/recipe-to-search';

export enum BindData {
  HideComments, ScrollTop, HideSearch
}
@Injectable({
  providedIn: 'root'
})
export class NavBarService {
  private activatedRecipe: Recipe = null;
  private commentActivated = false;
  private subject: Subject<any> = new Subject<any>();
  private searchSubject: Subject<any> = new Subject<any>();
  lastUrlBeforeSearch: string;

  constructor(private router: Router) { }

  emitEvent(data: any) {
    this.subject.next(data);
  }

  refreshSearchEvent(data: RecipeToSearch) {
    this.searchSubject.next(data);
  }

  isRecipeActivated() {
    return this.activatedRecipe !== null;
  }
  getRecipeNames() {
    if (this.isRecipeActivated()) {
      return `${this.activatedRecipe.name} ${(this.activatedRecipe.secondaryName ? this.activatedRecipe.secondaryName : '')}`;
    } else {
      return '';
    }
  }
  subscribe(subs: (data) => void): Subscription {
    return this.subject.subscribe(subs);
  }
  subscribeSearch(subs: (data: RecipeToSearch) => void): Subscription {
    return this.searchSubject.subscribe(subs);
  }
  setActivatedRecipe(recipe: Recipe) {
    this.activatedRecipe = recipe;
  }
  getActivatedRecipe() {
    return this.activatedRecipe;
  }
  setCommentActivated(activated: boolean) {
    setTimeout(() => {
      this.commentActivated = activated;
    });
  }
  isCommentActivated() {
    return this.commentActivated;
  }
  returnBackFromSearch() {
    console.log(this.lastUrlBeforeSearch);
    if (this.lastUrlBeforeSearch) {
      this.router.navigate([this.lastUrlBeforeSearch]).then();
    } else {
      this.router.navigate(['recipes']).then();
    }
  }
}
