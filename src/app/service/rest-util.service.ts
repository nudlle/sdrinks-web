import { Injectable } from '@angular/core';
import { RestServiceService } from './rest-service.service';
import { Recipe } from '../domain/recipe';
import { environment } from 'src/environments/environment';
import { RecipeToSearch } from '../domain/recipe-to-search';
import { UserComment } from '../domain/user-comment';
import { ServerResponseCode } from '../response/server-response-code';
import { UserMessage } from '../domain/user-message';
import { Subscription } from '../domain/subscription';
import { UserResponse } from '../response/user-response';
import {LoginService} from './login.service';
import {ImageUrls} from '../domain/image-urls';
import {Observable} from 'rxjs';
import {StringResponse} from '../response/string-response';

@Injectable({
  providedIn: 'root'
})
export class RestUtilService {
  public static handleError(errorCode: ServerResponseCode): string {
    switch (errorCode) {
      case ServerResponseCode.CHECK_EMAIL_FAILED:
        return 'Не верный проверочный код';
      case ServerResponseCode.UNIQUE_EMAIL_EXCEPTION:
        return 'Почтовый адрес занят';
      case ServerResponseCode.USER_NOT_FOUND:
        return 'Пользователь не найден';
      case ServerResponseCode.LOGIN_FAILED:
        return 'Не верный пароль';
      case ServerResponseCode.USER_MESSAGE_NOT_FOUND:
        return 'Ошибка приложения не найдена';
      case ServerResponseCode.COMMENT_ALREADY_POSTED:
        return 'Комментарий уже был опубликован';
      case ServerResponseCode.FILE_IS_TOO_BIG:
        return 'Превышен размер файла';
      case ServerResponseCode.COMMENT_NOT_FOUND:
        return 'Комментарий не найден';
      case ServerResponseCode.FILE_UPLOAD_FAILED:
        return 'Ошибка загрузки файла';
      case ServerResponseCode.IMAGE_RESIZE_FAILED:
        return 'Ошибка обработки изображения';
      case ServerResponseCode.RECIPE_NOT_FOUND:
        return 'Рецепт не найден';
      case ServerResponseCode.RESOURCE_NOT_FOUND:
        return 'Ресурс не найден';
      case ServerResponseCode.SUBSCRIPTION_NOT_FOUND:
        return 'Подписка не найдена';
      case ServerResponseCode.SUBSCRIPTION_NOT_VALID:
        return 'Закончился срок действия подписки';
      case ServerResponseCode.VALIDATION_EXCEPTION:
        return 'Не верные значения';
      case ServerResponseCode.UNKNOWN_EXCEPTION:
        return 'Неизвестная ошибка';
      default: return '';
    }
  }

  private impl = (res: any, success, errorCode) => {
    if (res.code === ServerResponseCode.OK_RESPONSE) {
      success(res.data);
    } else {
      errorCode(res.code);
    }
  }
  private implArray = (res: any, success, errorCode) => {
    res.forEach(data => {
      if (data.code === ServerResponseCode.OK_RESPONSE) {
        success(data.data);
      } else {
        errorCode(data.code);
      }
    });
  }

  constructor(private httpRest: RestServiceService, private loginService: LoginService) { }

  public getLoginService() { return this.loginService; }

  public getRouter() {
    return this.loginService.getRouter();
  }

  private getRecipes(page: number, size: number,
                     handleResponse: (data: string) => void,
                     exception?: (error: any) => void,
                     complete?: () => void) {
    this.httpRest.get(`${environment.recipes}/all/${page}/${size}`, handleResponse, null, exception, complete);
  }

  public getRecipesImpl(page: number, size: number,
                        callback: (data: Recipe) => void,
                        errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                        complete?: () => void) {
    this.getRecipes(page, size, res => { this.implArray(res, callback, errorCode); }, exception, complete);
  }

  private getRecipeById(id: string,
                        handleResponse: (data: string) => void,
                        exception?: (error: any) => void,
                        complete?: () => void) {
    this.httpRest.get(`${environment.recipes}/by-id/${id}`, handleResponse, null, exception, complete);
  }
  public getRecipeByIdImpl(id: string, callback: (data: Recipe) => void,
                           errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                           complete?: () => void) {
    this.getRecipeById(id, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private searchRecipes(recipeToSearch: RecipeToSearch, page: number, size: number,
                        handleResponse: (data: string) => void,
                        exception?: (error: any) => void,
                        complete?: () => void) {
    this.httpRest.put(`${environment.recipes}/se/${page}/${size}`, handleResponse, recipeToSearch,
      null, exception, complete);
  }
  public searchRecipesImpl(recipeToSearch: RecipeToSearch, page: number, size: number,
                           callback: (data: Recipe) => void,
                           errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                           complete?: () => void) {
    this.searchRecipes(recipeToSearch, page, size, res => { this.implArray(res, callback, errorCode); }, exception, complete);
  }
  private createRecipe(recipe: Recipe,
                       handleResponse: (data: string) => void,
                       exception?: (error: any) => void,
                       complete?: () => void) {
    this.httpRest.post(`${environment.recipes}/create`, handleResponse, recipe,
      this.loginService.getHeaders(), exception, complete);
  }
  public createRecipeImpl(recipe: Recipe, callback: (data: Recipe) => void,
                          errorCode: (code: ServerResponseCode) => void,
                          exception?: (error: any) => void,
                          complete?: () => void) {
    this.createRecipe(recipe, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private activateRecipe(recipeId: string, userId: string,
                         handleResponse: (data: string) => void,
                         exception?: (error: any) => void,
                         complete?: () => void) {
    this.httpRest.post(`${environment.recipes}/activate/${recipeId}/${userId}`, handleResponse, null,
      this.loginService.getHeaders(), exception, complete);
  }
  public activateRecipeImpl(recipeId: string, userId: string,
                            code: (code: ServerResponseCode) => void,
                            exception?: (error: any) => void,
                            complete?: () => void) {
    this.activateRecipe(recipeId, userId, res => { code(res); }, exception, complete);
  }
  private deleteRecipe(userId: string, recipeId: string,
                       handleResponse: (data: string) => void,
                       exception?: (error: any) => void,
                       complete?: () => void) {
    this.httpRest.delete(`${environment.recipes}/delete/${recipeId}/${userId}`, handleResponse,
      this.loginService.getHeaders(), exception, complete);
  }
  public deleteRecipeImpl(userId: string, recipeId: string,
                          code: (code: ServerResponseCode) => void,
                          exception?: (error: any) => void,
                          complete?: () => void) {
    this.deleteRecipe(userId, recipeId, res => { code(res); }, exception, complete);
  }
  private incRecipeViewed(recipeId: string,
                          handleResponse: (data: string) => void,
                          exception?: (error: any) => void,
                          complete?: () => void) {
    this.httpRest.put(`${environment.recipes}/inc/view/${recipeId}`, handleResponse,
      null, null, exception, complete);
  }
  public incRecipeViewedImpl(recipeId: string, code: (code: ServerResponseCode) => void,
                             exception?: (error: any) => void, complete?: () => void) {
    this.incRecipeViewed(recipeId, res => { code(res); }, exception, complete);
  }
  private incRecipeAdClicked(adId: string,
                             handleResponse: (data: string) => void,
                             exception?: (error: any) => void,
                             complete?: () => void) {
    this.httpRest.put(`${environment.recipes}/inc/click/${adId}`, handleResponse,
      null, null, exception, complete);
  }
  public incRecipeAdClickedImpl(adId: string, code: (code: ServerResponseCode) => void,
                                exception?: (error: any) => void, complete?: () => void) {
    this.incRecipeAdClicked(adId, res => { code(res); }, exception, complete);
  }
  private addLikedRecipe(userId: string, recipeId: string,
                         handleResponse: (data: string) => void,
                         exception?: (error: any) => void,
                         complete?: () => void) {
    this.httpRest.post(`${environment.users}/liked-recipe/${userId}/${recipeId}`, handleResponse, null,
      this.loginService.getHeaders(), exception, complete);
  }
  public addLikedRecipeImpl(userId: string, recipeId: string, code: (code: ServerResponseCode) => void,
                            exception?: (error: any) => void, complete?: () => void) {
    this.addLikedRecipe(userId, recipeId, res => { code(res); }, exception, complete);
  }
  private deleteLikedRecipe(userId: string, recipeId: string,
                            handleResponse: (data: string) => void,
                            exception?: (error: any) => void,
                            complete?: () => void) {
    this.httpRest.delete(`${environment.users}/liked-recipe/${userId}/${recipeId}`, handleResponse,
      this.loginService.getHeaders(), exception, complete);
  }
  public deleteLikedRecipeImpl(userId: string, recipeId: string,
                               code: (code: ServerResponseCode) => void,
                               exception?: (error: any) => void,
                               complete?: () => void) {
    this.deleteLikedRecipe(userId, recipeId, res => { code(res); }, exception, complete);
  }
  private createComment(comment: UserComment,
                        handleResponse: (data: string) => void,
                        exception?: (error: any) => void,
                        complete?: () => void) {
    this.httpRest.post(`${environment.comments}/create`, handleResponse, comment,
      this.loginService.getHeaders(), exception, complete);
  }
  public createCommentImpl(comment: UserComment, callback: (data: UserComment) => void,
                           errorCode: (code: ServerResponseCode) => void,
                           exception?: (error: any) => void,
                           complete?: () => void) {
    this.createComment(comment, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private getCommentsOfRecipe(recipeId: string, page: number, size: number,
                              handleResponse: (data: string) => void,
                              exception?: (error: any) => void,
                              complete?: () => void) {
    this.httpRest.get(`${environment.comments}/${recipeId}/${page}/${size}`, handleResponse,
      null, exception, complete);
  }
  public getCommentsOfRecipeImpl(recipeId: string, page: number, size: number, callback: (data: UserComment) => void,
                                 errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                                 complete?: () => void) {
    this.getCommentsOfRecipe(recipeId, page, size, res => { this.implArray(res, callback, errorCode); }, exception, complete);
  }
  private isCommentAlreadyPosted(recipeId: string, userId: string,
                                 handleResponse: (data: string) => void,
                                 exception?: (error: any) => void,
                                 complete?: () => void) {
    this.httpRest.get(`${environment.comments}/check/${recipeId}/${userId}`, handleResponse,
      this.loginService.getHeaders(), exception, complete);
  }
  public isCommentAlreadyPostedImpl(recipeId: string, userId: string,
                                    code: (code: ServerResponseCode) => void,
                                    exception?: (error: any) => void,
                                    complete?: () => void) {
    this.isCommentAlreadyPosted(recipeId, userId, res => { code(res); }, exception, complete);
  }
  private uploadRecipeImage(recipeId: string, file: FormData,
                            handleResponse: (data: string) => void,
                            exception?: (error: any) => void,
                            complete?: () => void) {
    this.httpRest.post(`${environment.uploads}/recipe-image/${recipeId}`, handleResponse, file,
      this.loginService.getHeaders(), exception, complete);
  }
  public uploadRecipeImageImpl(recipeId: string, file, callback: (data: ImageUrls) => void,
                               errorCode: (code: ServerResponseCode) => void,
                               exception?: (error: any) => void,
                               complete?: () => void) {
    this.uploadRecipeImage(recipeId, file, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private uploadRecipeStepImage(recipeId: string, stepId: string, file: FormData,
                                handleResponse: (data: string) => void,
                                exception?: (error: any) => void,
                                complete?: () => void) {
    this.httpRest.post(`${environment.uploads}/recipe-step/${recipeId}/${stepId}`, handleResponse, file,
      this.loginService.getHeaders(), exception, complete);
  }
  public uploadRecipeStepImageImpl(recipeId: string, stepId: string, file: FormData, callback: (data: string) => void,
                                   errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                                   complete?: () => void) {
    this.uploadRecipeStepImage(recipeId, stepId, file, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private uploadRecipeAdImage(recipeId: string, adId: string, file: FormData,
                              handleResponse: (res: UserResponse) => void,
                              handleError?: (error: any) => void,
                              completeSignal?: () => void) {
    this.httpRest.post(`${environment.uploads}/recipe-ad/${recipeId}/${adId}`, handleResponse, file,
      this.loginService.getHeaders(), handleError, completeSignal);
  }
  public uploadRecipeAdImageImpl(recipeId: string, adId: string, file: FormData, callback: (data: string) => void,
                                 errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                                 complete?: () => void) {
    this.uploadRecipeAdImage(recipeId, adId, file, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private uploadUserAvatar(userId: string, file: FormData,
                           handleResponse: (res: UserResponse) => void,
                           handleProgress: (progress: number) => void,
                           handleError?: (error: any) => void,
                           completeSignal?: () => void) {
    this.httpRest.postFile(`${environment.uploads}/user-avatar/${userId}`, handleResponse, file, handleProgress, {
      headers: this.loginService.getHeaders().headers,
      reportProgress: true,
      observe: 'events'
    }, handleError, completeSignal);
  }
  public uploadUserAvatarImpl(userId: string, file: FormData, callback: (data: string) => void,
                              handleProgress: (progress: number) => void,
                              errorCode: (code: ServerResponseCode) => void, exception?: (error: any) => void,
                              complete?: () => void) {
    this.uploadUserAvatar(userId, file, res => { this.impl(res, callback, errorCode); }, handleProgress, exception, complete);
  }
  private postUserMessageMessage(userMessage: UserMessage,
                                 handleResponse: (res: UserResponse) => void,
                                 handleError?: (error: any) => void,
                                 completeSignal?: () => void) {
    this.httpRest.post(`${environment.userMessages}/create`, handleResponse, userMessage, null, handleError, completeSignal);
  }
  public postUserMessageImpl(userMessage: UserMessage,
                             callback: (data: UserMessage) => void,
                             errorCode: (code: ServerResponseCode) => void,
                             exception?: (error: any) => void,
                             complete?: () => void) {
    this.postUserMessageMessage(userMessage, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private createSub(sub: Subscription,
                    handleResponse: (res: UserResponse) => void,
                    handleError?: (error: any) => void,
                    completeSignal?: () => void) {
    this.httpRest.post(`${environment.subscriptions}/create-sub`, handleResponse, sub,
      this.loginService.getHeaders(), handleError, completeSignal);
  }
  public createSubImpl(sub: Subscription, callback: (data: Subscription) => void,
                       errorCode: (code: ServerResponseCode) => void,
                       exception?: (error: any) => void,
                       complete?: () => void) {
    this.createSub(sub, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  private getUser(userId: string,
                  handleResponse: (res: UserResponse) => void,
                  handleError?: (error: any) => void,
                  completeSignal?: () => void) {
    this.httpRest.get(`${environment.users}/user/${userId}`, handleResponse,
      this.loginService.getHeaders(), handleError, completeSignal);
  }
  public getUserImpl(userId: string, callback: (data: any) => void,
                     errorCode: (code: ServerResponseCode) => void,
                     exception?: (error: any) => void,
                     complete?: () => void) {
    this.getUser(userId, res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
  public getUserImplObservable(userId: string): Observable<UserResponse> {
    return this.httpRest.directGet(`${environment.users}/user/${userId}`, this.loginService.getHeaders());
  }
  private getUsageRestrictions(handleResponse: (res: StringResponse) => void,
                               handleError?: (error: any) => void,
                               completeSignal?: () => void) {
    this.httpRest.get(`${environment.license}/usage-restrictions`, handleResponse, null, handleError, completeSignal);
  }
  public getUsageRestrictionsImpl(callback: (data: any) => void,
                                  errorCode: (code: ServerResponseCode) => void,
                                  exception?: (error: any) => void,
                                  complete?: () => void) {
    this.getUsageRestrictions(res => { this.impl(res, callback, errorCode); }, exception, complete);
  }
}
