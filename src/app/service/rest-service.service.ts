import { Injectable } from '@angular/core';
import {HttpClient, HttpEventType, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class RestServiceService {
  constructor(private http: HttpClient, private router: Router) {
  }

  private impl(obs: Observable<any>,
               handleResponse: (res: any) => void,
               handleError?: (error: any) => void,
               completeSignal?: () => void) {
    obs.subscribe(handleResponse, error => {
      if (handleError) {
        handleError(error);
      } else {
        this.handleError(error);
      }
    }, completeSignal);
  }

  private implFile(obs: Observable<any>,
                   handleResponse: (res: any) => void,
                   handleProgress?: (progress: number) => void,
                   handleError?: (error: any) => void,
                   completeSignal?: () => void) {
    obs.pipe(map((event) => {
        switch (event.type) {
          case HttpEventType.DownloadProgress:
          case HttpEventType.UploadProgress:
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', message: progress };
          case HttpEventType.Response:
            return event.body;
          default:
            return `Unhandled event: ${event.type}`;
        }
      })
    ).subscribe(res => {
      if (res.status === 'progress') {
        handleProgress(res.message);
      } else {
        handleResponse(res);
      }
    }, error => {
      if (handleError) {
        handleError(error);
      } else {
        this.handleError(error);
      }
    }, completeSignal);
  }

  public directGet(url: string, options?: any): Observable<any> {
      options = !options ? {headers: new HttpHeaders()} : options;
      return this.http.get(`${environment.serverUrl}${url}`, options);
  }

  public directPost(url: string, data: any, options?: any): Observable<any> {
      options = !options ? {headers: new HttpHeaders()} : options;
      return this.http.post(`${environment.serverUrl}${url}`, data, options);
  }
  public directPut(url: string, data: any, options?: any): Observable<any> {
    options = !options ? {headers: new HttpHeaders()} : options;
    return this.http.put(`${environment.serverUrl}${url}`, data, options);
  }
  public directDelete(url: string, data: any, options?: any): Observable<any> {
    options = !options ? {headers: new HttpHeaders()} : options;
    return this.http.put(`${environment.serverUrl}${url}`, data, options);
  }

  public get(url: string, handleResponse: (res: any) => void, options?: any,
             handleError?: (error: any) => void, completeSignal?: () => void) {
      options = !options ? {headers: new HttpHeaders()} : options;
      const obs = this.http.get(`${environment.serverUrl}${url}`, options);
      this.impl(obs, handleResponse, handleError, completeSignal);
  }

  public post(url: string, handleResponse: (res: any) => void, json?: any, options?: any,
              handleError?: (error: any) => void, completeSignal?: () => void) {
      options = !options ? {headers: new HttpHeaders()} : options;
      const obs = this.http.post(`${environment.serverUrl}${url}`, json, options);
      this.impl(obs, handleResponse, handleError, completeSignal);
  }
  public postFile(url: string, handleResponse: (res: any) => void, data?: any,
                  handleProgress?: (progress: number) => void, options?: any,
                  handleError?: (error: any) => void, completeSignal?: () => void) {
    options = !options ? {
      headers: new HttpHeaders(),
      reportProgress: true,
      observe: 'events'
    } : options;
    const obs = this.http.post(`${environment.serverUrl}${url}`, data, options);
    this.implFile(obs, handleResponse, handleProgress, handleError, completeSignal);
  }

  public put(url: string, handleResponse: (res: any) => void, json?: any, options?: any,
             handleError?: (error: any) => void, completeSignal?: () => void) {
      options = !options ? {headers: new HttpHeaders()} : options;
      const obs = this.http.put(`${environment.serverUrl}${url}`, json, options);
      this.impl(obs, handleResponse, handleError, completeSignal);
  }

  public delete(url: string, handleResponse: (res: any) => void, options?: any,
                handleError?: (error: any) => void, completeSignal?: () => void) {
    options = !options ? {headers: new HttpHeaders()} : options;
    const obs = this.http.delete(`${environment.serverUrl}${url}`, options);
    this.impl(obs, handleResponse, handleError, completeSignal);
  }

  public navigate(params: any) {
      return this.router.navigate(params);
  }

  private handleError(error: any) {
      switch (error.status) {
          case 401: {
              this.navigate(['auth']).then();
              break;
          }
          case 404: {
              this.navigate(['notfound']).then();
              break;
          }
          case 500: {
              this.navigate(['error']).then();
              break;
          }
      }
  }
}
