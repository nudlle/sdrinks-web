import {Injectable} from '@angular/core';
import {User} from '../domain/user';
import {HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {SessionToken} from '../security/session-token';
import {UserRequest} from '../security/user-request';
import {ServerResponseCode} from '../response/server-response-code';
import {environment} from '../../environments/environment';
import {RestServiceService} from './rest-service.service';
import {FormControl} from '@angular/forms';

@Injectable()
export class LoginService {
  public static userLoggedIn = false;
  public static redirectTo = null;
  user: User;
  sessionToken: SessionToken;

  constructor(private http: RestServiceService, private router: Router) {
    const userId = localStorage.getItem('userId');
    const t = localStorage.getItem('token');
    const time = localStorage.getItem('timestamp');
    if (userId && t && time) {
      this.sessionToken = {
        id: userId,
        token: t,
        timestamp: Number(time)
      };
    }
  }

  private static getUserRequest(em: string, p: string): UserRequest {
    const u: User = {
      userId: undefined,
      email: em,
      password: p,
      avatarUrl: null,
      blacklist: null,
      ownRecipes: null,
      likedRecipes: null,
      userSubscription: null
    };
    return {user: u, timestamp: null};
  }
  private static getUserLoginRequest(uId: string, userPass: string) {
    const u: User = {
      userId: uId,
      email: null,
      password: userPass,
      avatarUrl: null,
      blacklist: null,
      ownRecipes: null,
      likedRecipes: null,
      userSubscription: null
    };
    return {user: u, timestamp: null};
  }

  static handleErrorResponse(rc: ServerResponseCode, f1?: FormControl, f2?: FormControl) {
    switch (rc) {
      case ServerResponseCode.USER_NOT_FOUND:
        if (f1) {
          f1.setErrors({user_not_found: true});
        }
        break;
      case ServerResponseCode.LOGIN_FAILED:
        if (f2) {
          f2.setErrors({login_failed: true});
        }
        break;
      case ServerResponseCode.UNIQUE_EMAIL_EXCEPTION:
        if (f2) {
          f2.setErrors({email_already_exist: true});
        }
        break;
      case ServerResponseCode.CHECK_EMAIL_FAILED:
        if (f1) {
          f1.setErrors({bad_check_code: true});
        }
        break;
    }
  }

  getRouter() {
    return this.router;
  }

  getHeaders() {
    if (this.sessionToken) {
      return {
        headers: new HttpHeaders({
            Authorization: 'Basic ' + btoa(`${this.sessionToken.id}:${this.sessionToken.token}`), 'X-Requested-With': 'XMLHttpRequest'
        })
      };
    } else {
      return {
        headers: new HttpHeaders()
      };
    }
  }
  private login(userId: string, userPass: string) {
    const ur = LoginService.getUserLoginRequest(userId, userPass);
    this.llogin(ur);
  }
  recoverByLoginAndPassword(f1: FormControl, f2: FormControl) {
    const ur = LoginService.getUserRequest(f1.value, f2.value);
    this.getUserByPassword(ur, f1, f2);
  }
  recoverByEmailRequest(f1: FormControl, showConfirm: (show: boolean) => void) {
    this.recoverEmailHashRequest(f1, showConfirm);
  }
  recoverByEmailImpl(f1: FormControl, pass: string, showSendAgain: (show: boolean) => void) {
    this.recoverByEmail(f1, pass, showSendAgain);
  }
  checkUserEmail(f1: FormControl, pass: string) {
    const ur = LoginService.getUserRequest(f1.value, pass);
    this.user = ur.user;
    this.sendCheckEmail(f1.value, f1);
  }
  reSendCheckUserEmail(f1: FormControl, showSend: (show: boolean) => void) {
    this.reSendCheckEmail(this.user.email, f1, showSend);
  }
  reSendRecoverEmailRequest(f1: FormControl, showSend: (show: boolean) => void) {
    this.reSendEmailHashRequest(this.user.email, f1, showSend);
  }
  registerImpl(emailHash: FormControl, showSend: (show: boolean) => void) {
    const pass = this.user.password;
    this.register(this.user.email, emailHash, pass, LoginService.getUserRequest(this.user.email, this.user.password), showSend);
  }

  public llogin(userRequest: UserRequest) {
    this.http.post(`${environment.auth}/login`, res => {
      if (res.code === ServerResponseCode.OK_RESPONSE) {
        this.sessionToken = res.data;
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('timestamp', String(res.data.timestamp));
        LoginService.userLoggedIn = true;
        if (LoginService.redirectTo) {
          this.router.navigate([LoginService.redirectTo]).then();
          LoginService.redirectTo = null;
        } else {
          this.router.navigate(['profile']).then();
        }
      } else {
        LoginService.handleErrorResponse(res.code);
      }
    }, userRequest);
  }

  private handleRegisterRecoverResponse = (res, pass: string, f1: FormControl, f2: FormControl,
                                           showSendAgainFunc?: (show: boolean) => void) => {
    if (res.code === ServerResponseCode.OK_RESPONSE) {
      this.user = res.data;
      localStorage.setItem('userId', this.user.userId);
      this.login(this.user.userId, pass);
    } else {
      if (showSendAgainFunc) {
        showSendAgainFunc(true);
      }
      LoginService.handleErrorResponse(res.code, f1, f2);
    }
  }

  private register(email: string, emailHash: FormControl, pass: string, userRequest: UserRequest, showSendAgain: (show: boolean) => void) {
    this.http.post(`${environment.auth}/register/${email}/${emailHash.value}`,
        res => this.handleRegisterRecoverResponse(res, pass, emailHash, null, showSendAgain), userRequest);
  }

  private getUserByPassword(userRequest: UserRequest, f1: FormControl, f2: FormControl) {
    this.http.post(`${environment.auth}/recover`,
        res => this.handleRegisterRecoverResponse(res, f2.value, f1, f2, null), userRequest);
  }
  public logout(userId: string) {
    this.http.post(`${environment.auth}/logout/${userId}`, res => {
      if (res === ServerResponseCode.OK_RESPONSE) {
        localStorage.clear();
        this.user = null;
        LoginService.userLoggedIn = false;
        this.router.navigate(['/auth']).then();
      }
    });
  }
  private sendCheckEmail(email: string, f1: FormControl) {
    this.http.get(`${environment.auth}/checkEmail/${email}`, res => {
      if (res === ServerResponseCode.OK_RESPONSE) {
        this.router.navigate(['auth/confirm']).then();
      } else {
        LoginService.handleErrorResponse(res, f1, null);
      }
    });
  }
  private reSendCheckEmail(email: string, f1: FormControl, showSend: (show: boolean) => void) {
    this.http.get(`${environment.auth}/checkEmail/${email}`, res => {
      if (res === ServerResponseCode.OK_RESPONSE) {
        showSend(false);
      } else {
        LoginService.handleErrorResponse(res, f1, null);
      }
    });
  }
  private recoverEmailHashRequest(f1: FormControl, showConfirm: (show: boolean) => void) {
    this.user = LoginService.getUserRequest(f1.value, '').user;
    this.http.get(`${environment.auth}/recover-request/${f1.value}`, res => {
      if (res === ServerResponseCode.OK_RESPONSE) {
        showConfirm(true);
      } else {
        LoginService.handleErrorResponse(res, f1, null);
      }
    });
  }
  private reSendEmailHashRequest(email: string, f1: FormControl, showSend: (show: boolean) => void) {
    this.http.get(`${environment.auth}/recover-request/${email}`, res => {
      if (res === ServerResponseCode.OK_RESPONSE) {
        showSend(false);
      } else {
        LoginService.handleErrorResponse(res, f1, null);
      }
    });
  }
  private recoverByEmail(f1: FormControl, newPass: string, showSendAgain: (show: boolean) => void) {
    const ur = LoginService.getUserRequest(this.user.email, newPass);
    this.http.post(`${environment.auth}/recover/${f1.value}`, res =>
      this.handleRegisterRecoverResponse(res, newPass, f1, null, showSendAgain), ur);
  }
}
