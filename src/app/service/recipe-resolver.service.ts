import { Injectable } from '@angular/core';
import {DrinkTaste} from '../domain/drink-taste';
import {DrinkType} from '../domain/drink-type';
import {DrinkState} from '../domain/drink-state';
import {Recipe} from '../domain/recipe';
import {environment} from '../../environments/environment';
import {AppComponent} from '../app.component';
import {RecipeStepCook} from '../domain/recipe-step-cook';
import {Ad} from '../domain/ad';
import {User} from '../domain/user';

export enum RecipeImageType {
  LARGE, MEDIUM, SMALL
}
@Injectable({
  providedIn: 'root'
})
export class RecipeResolverService {

  constructor() { }

  public static getRecipeTaste(taste: DrinkTaste, showLight: boolean = true) {
    if (AppComponent.isDarkTheme || showLight) {
      switch (taste) {
        case DrinkTaste.Berries: {
          return 'ic_berries_dark';
        }
        case DrinkTaste.Fruit: {
          return 'ic_fruit_dark';
        }
        case DrinkTaste.Herb: {
          return 'ic_herb_dark';
        }
        case DrinkTaste.Sour: {
          return 'ic_sour_dark';
        }
        case DrinkTaste.Spicy: {
          return 'ic_spicy_dark';
        }
        case DrinkTaste.Tasty: {
          return 'ic_sweet_dark';
        }
        case DrinkTaste.Vegetable: {
          return 'ic_vegetables_dark';
        }
      }
    } else {
      switch (taste) {
        case DrinkTaste.Berries: {
          return 'ic_berries_light';
        }
        case DrinkTaste.Fruit: {
          return 'ic_fruit_light';
        }
        case DrinkTaste.Herb: {
          return 'ic_herb_light';
        }
        case DrinkTaste.Sour: {
          return 'ic_sour_light';
        }
        case DrinkTaste.Spicy: {
          return 'ic_spicy_light';
        }
        case DrinkTaste.Tasty: {
          return 'ic_sweet_light';
        }
        case DrinkTaste.Vegetable: {
          return 'ic_vegetables_light';
        }
      }
    }
  }
  public static getRecipeTasteText(taste: DrinkTaste) {
    switch (taste) {
      case DrinkTaste.Berries: {
        return 'ягодный';
      }
      case DrinkTaste.Fruit: {
        return 'фруктовый';
      }
      case DrinkTaste.Herb: {
        return 'травяной';
      }
      case DrinkTaste.Sour: {
        return 'кислый';
      }
      case DrinkTaste.Spicy: {
        return 'острый';
      }
      case DrinkTaste.Tasty: {
        return 'сладкий';
      }
      case DrinkTaste.Vegetable: {
        return 'овощной';
      }
    }
  }

  public static getDrinkTaste(index: number) {
    switch (index) {
      case 0: { return DrinkTaste.Tasty; }
      case 1: { return DrinkTaste.Spicy; }
      case 2: { return DrinkTaste.Fruit; }
      case 3: { return DrinkTaste.Vegetable; }
      case 4: { return DrinkTaste.Sour; }
      case 5: { return DrinkTaste.Berries; }
      case 6: { return DrinkTaste.Herb; }
    }
  }

  public static getRecipeType(type: DrinkType, showLight: boolean = true) {
    if (AppComponent.isDarkTheme || showLight) {
      switch (type) {
        case DrinkType.Tea:
        case DrinkType.Coffee: {
          return 'ic_tea_coffee_dark';
        }
        case DrinkType.Cocktail: {
          return 'ic_cocktail_dark';
        }
        case DrinkType.Lemonade: {
          return 'ic_lemonade_dark';
        }
        case DrinkType.Liqueur: {
          return 'ic_liqueur_dark';
        }
        case DrinkType.Milkshake: {
          return 'ic_milkshake_dark';
        }
        case DrinkType.Shot: {
          return 'ic_shot_dark';
        }
      }
    } else {
      switch (type) {
        case DrinkType.Tea:
        case DrinkType.Coffee: {
          return 'ic_tea_coffee_light';
        }
        case DrinkType.Cocktail: {
          return 'ic_cocktail_light';
        }
        case DrinkType.Lemonade: {
          return 'ic_lemonade_light';
        }
        case DrinkType.Liqueur: {
          return 'ic_liqueur_light';
        }
        case DrinkType.Milkshake: {
          return 'ic_milkshake_light';
        }
        case DrinkType.Shot: {
          return 'ic_shot_light';
        }
      }
    }
  }

  public static getRecipeTypeText(type: DrinkType) {
    switch (type) {
      case DrinkType.Tea:
        return 'чайный';
      case DrinkType.Coffee: {
        return 'кофейный';
      }
      case DrinkType.Cocktail: {
        return 'коктейль';
      }
      case DrinkType.Lemonade: {
        return 'лимонад';
      }
      case DrinkType.Liqueur: {
        return 'ликёр';
      }
      case DrinkType.Milkshake: {
        return 'молочный коктейль';
      }
      case DrinkType.Shot: {
        return 'шот';
      }
    }
  }

  public static getRecipeState(state: DrinkState, showLight: boolean = true) {
    if (AppComponent.isDarkTheme || showLight) {
      switch (state) {
        case DrinkState.Cold: {
          return 'ic_cold_drink_dark';
        }
        case DrinkState.Hot: {
          return 'ic_hot_drink_dark';
        }
        case DrinkState.Unspecified: {
          return '';
        }
      }
    } else {
      switch (state) {
        case DrinkState.Cold: {
          return 'ic_cold_drink_light';
        }
        case DrinkState.Hot: {
          return 'ic_hot_drink_light';
        }
        case DrinkState.Unspecified: {
          return '';
        }
      }
    }
  }

  public static getRecipeStateText(state: DrinkState) {
    switch (state) {
      case DrinkState.Cold: {
        return 'холодный';
      }
      case DrinkState.Hot: {
        return 'горячий';
      }
      case DrinkState.Unspecified: {
        return '';
      }
    }
  }

  public static transformUserEmail(email: string) {
    const index = email.indexOf('@') + 1;
    return email.substring(0, index);
  }

  public static getRecipeImageSource(recipe: Recipe, type: RecipeImageType = RecipeImageType.LARGE) {
    switch (type) {
      case RecipeImageType.LARGE: {
        return `${environment.serverUrl}${environment.resources}/${environment.recipes}/${recipe.recipeId}/${recipe.imageUrls.large}`;
      }
      case RecipeImageType.MEDIUM: {
        return `${environment.serverUrl}${environment.resources}/${environment.recipes}/${recipe.recipeId}/${recipe.imageUrls.medium}`;
      }
      case RecipeImageType.SMALL: {
        return `${environment.serverUrl}${environment.resources}/${environment.recipes}/${recipe.recipeId}/${recipe.imageUrls.small}`;
      }
    }
  }

  public static getRecipeStepImageSource(recipeStep: RecipeStepCook) {
    return `${environment.serverUrl}${environment.resources}/${environment.recipes}/${recipeStep.recipeId}/${recipeStep.recipeStepCookId}/${recipeStep.imageUrl}`;
  }

  public static getRecipeAdImageSource(ad: Ad) {
    return `${environment.serverUrl}${environment.resources}/${environment.ad}/${ad.recipeId}/${ad.adId}/${ad.imageUrl}`;
  }

  public static loadFile(event: FileList, callback: (result) => void, callback2: (item) => void) {
    const reader = new FileReader();
    reader.onload = () => {
      callback(reader.result);
    };
    reader.readAsDataURL(event.item(0));
    callback2(event.item(0));
  }

  public static checkSub(user: User) {
    if (user) {
      const userSub = user.userSubscription;
      return userSub && userSub.activated && userSub.endDate > new Date();
    }
    return false;
  }
}
