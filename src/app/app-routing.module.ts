import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RecipesComponent} from './recipes/recipes.component';
import {RecipeComponent} from './recipe/recipe.component';
import {ProfileComponent} from './profile/profile.component';
import {AboutComponent} from './about/about.component';
import {CreateRecipeComponent} from './create-recipe/create-recipe.component';
import {MainComponent} from './main/main.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {RecoverComponent} from './recover/recover.component';
import {ConfirmEmailComponent} from './confirm-email/confirm-email.component';
import {AuthGuard} from './guard/auth.guard';
import {ProfileGuard} from './guard/profile.guard';
import {UserRecipesComponent} from './user-recipes/user-recipes.component';
import {ExitPreviewRecipeGuard} from './guard/exit-preview-recipe.guard';
import {ExitCreateRecipeGuard} from './guard/exit-create-recipe.guard';
import {BuySubscriptionComponent} from './buy-subscription/buy-subscription.component';
import {ExitBuySubscriptionGuard} from './guard/exit-buy-subscription.guard';
import {SearchResultComponent} from './search-result/search-result.component';


const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'recipes', component: RecipesComponent},
  {path: 'recipes/show/:id', component: RecipeComponent},
  {path: 'recipes/create', component: CreateRecipeComponent, canDeactivate: [ExitCreateRecipeGuard]},
  {path: 'recipes/create/preview/:id', component: RecipeComponent, canDeactivate: [ExitPreviewRecipeGuard]},
  {path: 'added', component: UserRecipesComponent},
  {path: 'liked', component: UserRecipesComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [ProfileGuard]},
  {path: 'buy_subscription', component: BuySubscriptionComponent, canActivate: [ProfileGuard],
    canDeactivate: [ExitBuySubscriptionGuard]},
  {path: 'about', component: AboutComponent},
  {path: 'auth', component: LoginComponent, canActivate: [AuthGuard]},
  {path: 'auth/register', component: RegisterComponent, canActivate: [AuthGuard]},
  {path: 'auth/recover', component: RecoverComponent, canActivate: [AuthGuard]},
  {path: 'auth/confirm', component: ConfirmEmailComponent, canActivate: [AuthGuard]},
  {path: 'search-result', component: SearchResultComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
