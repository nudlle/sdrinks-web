import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from '../service/login.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ErrorMessages} from '../utils/error-messages';
import {Utils} from '../utils/utils';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit, OnDestroy {
  emailSub: any;
  emailHashSub: any;
  confirmPassSub: any;
  recoverEmailForm: FormGroup;
  changePassForm: FormGroup;
  hide = true;
  hideConfirm = true;
  email: FormControl;
  emailHash: FormControl;
  password: FormControl;
  confirmPassword: FormControl;
  showConfirm = false;
  showSendAgain = false;

  constructor(private login: LoginService, private router: Router) {
  }
  initControls() {
    this.email = new FormControl('',
      [Validators.required, Validators.minLength(3), Validators.maxLength(128), Validators.email]);
    this.emailHash = new FormControl('', Validators.required);
    this.password = new FormControl('',
      [Validators.required, Validators.minLength(3), Validators.maxLength(128)]);
    this.confirmPassword = new FormControl('',
      [Validators.required, Validators.minLength(3), Validators.maxLength(128)]);
  }

  initForms() {
    this.recoverEmailForm = new FormGroup({
      email: this.email,
    });
    this.changePassForm = new FormGroup({
      emailHash: this.emailHash,
      password: this.password,
      confirmPassword: this.confirmPassword
    });
  }

  ngOnInit() {
    this.initControls();
    this.initForms();
    this.emailSub = this.email.valueChanges.subscribe(() => {
      if (this.password.hasError('user_not_found')) {
        this.password.setErrors(null);
      }
    });
    this.confirmPassSub = this.confirmPassword.valueChanges.subscribe(() => {
      if (this.confirmPassword.hasError('pass_not_eq')) {
        this.confirmPassword.setErrors(null);
      }
    });
    this.emailHashSub = this.emailHash.valueChanges.subscribe(() => {
      if (this.emailHash.hasError('bad_check_code')) {
        this.emailHash.setErrors(null);
      }
    });
  }
  goBack() {
    if (this.showConfirm) {
      this.showConfirm = false;
    } else {
      this.router.navigate(['auth']).then();
    }
  }

  recoverRequest() {
    if (Utils.checkWhitespaces(this.email.value)) {
      this.email.setValue('');
      this.email.markAsTouched();
    } else if (this.recoverEmailForm.valid) {
      this.login.recoverByEmailRequest(this.email, showConfirm => { this.showConfirm = showConfirm; });
    }
  }
  recover() {
    if (Utils.checkWhitespaces(this.emailHash.value)) {
      this.emailHash.setValue('');
      this.emailHash.markAsTouched();
    } else if (Utils.checkWhitespaces(this.password.value)) {
      this.password.setValue('');
      this.password.markAsTouched();
    } else if (Utils.checkWhitespaces(this.confirmPassword.value)) {
      this.confirmPassword.setValue('');
      this.confirmPassword.markAsTouched();
    } else if (this.changePassForm.valid) {
      if (this.password.value !== this.confirmPassword.value) {
        this.confirmPassword.setErrors({pass_not_eq: true});
      } else {
        this.login.recoverByEmailImpl(this.emailHash, this.password.value, show => this.showSendAgain = show);
      }
    }
  }
  sendAgainConfirmEmail() {
    this.login.reSendRecoverEmailRequest(this.emailHash, show => this.showSendAgain = show);
  }
  getErrorMsgEmail() {
    return this.email.hasError('required') ? ErrorMessages.ErrorRequired :
      this.email.hasError('email') ? ErrorMessages.ErrorEmail :
        this.email.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
          this.email.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
              this.email.hasError('user_not_found') ? ErrorMessages.ErrorUserNotFound : '';
  }
  getErrorMsgCheckEmail() {
    return this.emailHash.hasError('required') ? ErrorMessages.ErrorRequired :
      this.emailHash.hasError('bad_check_code') ? ErrorMessages.errorValidate('не верный код') : '';
  }
  getErrorMsgPassword() {
    return this.password.hasError('required') ? ErrorMessages.ErrorRequired :
      this.password.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
        this.password.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) : '';
  }
  getErrorConfirmMsgPassword() {
    return this.confirmPassword.hasError('required') ? ErrorMessages.ErrorRequired :
      this.confirmPassword.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
        this.confirmPassword.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
          this.confirmPassword.hasError('pass_not_eq') ? ErrorMessages.errorValidate('пароли не совпадают') : '';
  }

  ngOnDestroy(): void {
    if (this.emailHashSub) {
      this.emailHashSub.unsubscribe();
    }
    if (this.emailSub) {
      this.emailSub.unsubscribe();
    }
    if (this.confirmPassSub) {
      this.confirmPassSub.unsubscribe();
    }
  }
}
