import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../service/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorMessages} from '../utils/error-messages';
import {Utils} from '../utils/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  emailSub: any;
  loginForm: FormGroup;
  hide = true;
  email: FormControl;
  password: FormControl;

  initControls() {
    this.email = new FormControl('',
      [Validators.required, Validators.minLength(3), Validators.maxLength(100), Validators.email]);
    this.password = new FormControl('',
      [Validators.required, Validators.minLength(3), Validators.maxLength(128)]);
  }

  initForm() {
    this.loginForm = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
    this.initControls();
    this.initForm();
    this.emailSub = this.email.valueChanges.subscribe(() => {
      if (this.password.hasError('login_failed')) {
        this.password.setErrors(null);
      } else if (this.email.hasError('user_not_found')) {
        this.email.setErrors(null);
      }
    });
  }

  ngOnDestroy() {
    this.emailSub.unsubscribe();
  }

  getErrorMsgEmail() {
    return this.email.hasError('required') ? ErrorMessages.ErrorRequired :
      this.email.hasError('email') ? ErrorMessages.ErrorEmail :
        this.email.hasError('minlength') ? ErrorMessages.errorMinLength(3) :
          this.email.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
            this.email.hasError('user_not_found') ? ErrorMessages.ErrorUserNotFound : '';
  }

  getErrorMsgPassword() {
    return this.password.hasError('required') ? ErrorMessages.ErrorRequired :
      this.password.hasError('minlength') ? ErrorMessages.errorMinLength(3)  :
        this.password.hasError('maxlength') ? ErrorMessages.errorMaxLength(128) :
          this.password.hasError('login_failed') ? ErrorMessages.errorValidate('не верный пароль') : '';
  }

  login() {
    if (Utils.checkWhitespaces(this.email.value)) {
      this.email.setValue('');
      this.email.markAsTouched();
    } else if (Utils.checkWhitespaces(this.password.value)) {
      this.password.setValue('');
      this.password.markAsTouched();
    } else if (this.loginForm.valid) {
      this.loginService.recoverByLoginAndPassword(this.email, this.password);
    }
  }
  register() {
    this.router.navigate(['auth/register']).then();
  }
  recover() {
    this.router.navigate(['auth/recover']).then();
  }
}
