import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {RestUtilService} from '../service/rest-util.service';
import {LoginService} from '../service/login.service';
import {ServerResponseCode} from '../response/server-response-code';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {
  constructor(private restUtil: RestUtilService, private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (LoginService.userLoggedIn) {
      return true;
    } else {
      const userId = localStorage.getItem('userId');
      if (!userId) {
        this.router.navigate(['auth']).then();
        return false;
      } else {
        return this.restUtil.getUserImplObservable(userId).toPromise().then(res => {
          if (res.code === ServerResponseCode.OK_RESPONSE) {
            return true;
          } else {
            localStorage.clear();
            this.router.navigate(['auth']).then();
            return false;
          }
        }).catch((() => {
          localStorage.clear();
          this.router.navigate(['auth']).then();
          return false;
        }));
      }
    }
  }
}
