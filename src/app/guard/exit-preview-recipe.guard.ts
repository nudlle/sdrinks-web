import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {RecipeComponent} from '../recipe/recipe.component';

@Injectable({
  providedIn: 'root'
})
export class ExitPreviewRecipeGuard implements CanDeactivate<RecipeComponent> {
  canDeactivate(component: RecipeComponent, currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log('Call can deactivate in ExitPreviewRecipeGuard');
    if (!component.canExit()) {
      return window.confirm('Вы уверены? Данный рецепт не будет сохранён');
    } else {
      return true;
    }
  }
}
