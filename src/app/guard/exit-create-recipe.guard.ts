import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {CreateRecipeComponent} from '../create-recipe/create-recipe.component';

@Injectable({
  providedIn: 'root'
})
export class ExitCreateRecipeGuard implements CanDeactivate<CreateRecipeComponent> {
  canDeactivate(component: CreateRecipeComponent, currentRoute: ActivatedRouteSnapshot,
                currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!component.canExit()) {
      return window.confirm('Вы уверены? Данные рецепта будут потеряны');
    } else {
      return true;
    }
  }

}
