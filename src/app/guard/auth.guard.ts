import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {LoginService} from '../service/login.service';
import {RestUtilService} from '../service/rest-util.service';
import {ServerResponseCode} from '../response/server-response-code';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private restUtil: RestUtilService, private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (LoginService.userLoggedIn) {
      return false;
    } else {
      const userId = localStorage.getItem('userId');
      if (!userId) {
        return true;
      } else {
        return this.restUtil.getUserImplObservable(userId).toPromise().then(res => {
          if (res.code !== ServerResponseCode.OK_RESPONSE) {
            localStorage.clear();
            return true;
          } else {
            this.router.navigate(['profile']).then();
            return false;
          }
        }).catch((() => {
          localStorage.clear();
          return true;
        }));
      }
    }
  }
}
